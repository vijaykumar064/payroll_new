async function modal(){

    const res = await getauthdata();
    console.log(res);
    if(res.totalSigns===0){
        document.getElementById('piechart1').style.display='none';
    }
    else{
        document.getElementById('empty1').style.display='none';
    }
    document.getElementById("Authsigns").innerText=res.totalSigns;
    const res1=await getissuerdata();
    console.log(res1);
    if(res1.totalPayslipsIssued===0){
        document.getElementById('piechart2').style.display='none';
    }
    else{
        document.getElementById('empty2').style.display='none';
    }
    document.getElementById("totalCerts").innerText=res1.totalPayslipsIssued;

    // {"totalIssuers":10,"totalPayslipsIssued":1,"issuers":[{"iid":"1","issueCount":0},{"iid":"2","issueCount":1},{"iid":"3","issueCount":0},{"iid":"4","issueCount":0},{"iid":"5","issueCount":0},{"iid":"6","issueCount":0},{"iid":"7","issueCount":0},{"iid":"8","issueCount":0},{"iid":"9","issueCount":0},{"iid":"10","issueCount":0}],"isSuccess":true,"success":true}
   issuers=[];
  issuer=new Array(res1.totalIssuers);
  issuer[0]=new Array
  issuer[0][0]='Language'
  issuer[0][1]='Speakers (in millions)'
    for(i=1;i<=res1.totalIssuers;i++){
        issuer[i]=new Array
        // for(j=0;j<2;j++){

            issuer[i][0]=res1.issuers[i-1].iid;
            issuer[i][1]=res1.issuers[i-1].issueCount;
            // /issuer[i][1]=10;
        // }
      
        // issuers[i]=issuer;
        // console.log(issuers);
    }
    console.log(issuer)
    // {"totalAuthorizers":9,"authorizers":[{"aid":"1","authorizedCount":0},{"aid":"2","authorizedCount":0},{"aid":"3","authorizedCount":0},{"aid":"4","authorizedCount":0},{"aid":"5","authorizedCount":0},{"aid":"6","authorizedCount":0},{"aid":"7","authorizedCount":0},{"aid":"8","authorizedCount":0},{"aid":"9","authorizedCount":0}],"totalSigns":0,"isSuccess":true,"success":true}
    authorizers=new Array(res.totalAuthorizers);
    auth=new Array(res.totalAuthorizers);
    auth[0]=new Array
    auth[0][0]='Language'
    auth[0][1]='Speakers (in millions)'
      for(i=1;i<=res.totalAuthorizers;i++){
          auth[i]=new Array
              auth[i][0]=res.authorizers[i-1].aid;
                auth[i][1]=res.authorizers[i-1].authorizedCount;
            //   auth[i][1]=20;
      }
      authorizers=auth;
      console.log(authorizers)

         google.charts.load("current", {packages:["corechart"]});

      google.charts.setOnLoadCallback(drawissuerChart);

      function drawissuerChart() {

        var data = google.visualization.arrayToDataTable(issuer);
        var options = {

          title: 'Payslips by Issuers',

          legend: 'none',

          pieSliceText: 'label',

          pieHole: 0.6,

          slices: {  4: {offset: 0.0},

                    // 12: {offset: 0.3},

                    // 14: {offset: 0.4},

                    // 15: {offset: 0.5},

          },

        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

        chart.draw(data, options);

      }


      google.charts.load("current", {packages:["corechart"]});

      google.charts.setOnLoadCallback(drawauthChart);

      function drawauthChart() {

        var data = google.visualization.arrayToDataTable(auth);
        var options = {

          title: 'Signs by Authorizers',

          legend: 'none',

          pieSliceText: 'label',

          pieHole: 0.6,

          slices: {  4: {offset: 0.0},

                    // 12: {offset: 0.3},

                    // 14: {offset: 0.4},

                    // 15: {offset: 0.5},

          },

        };



        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);

      }


    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart2);

    function drawChart2() {
        values=[
            ['dapps', 'issuer', 'auth'],
            ['JAN', 80, 20],
            ['FEB', 40, 10],
            ['MAR', 40, 30],
            ['APR', 70, 40],
            ['MAY', 30, 15],
            ['JUN', 50, 20],
            ['JUL', 40, 34],
            ['AUG', 70, 46],
            ['SEP', 40, 46],
            ['OCT', 40, 25],
            ['NOV', 60, 36],
            ['DEC', 50, 48],
        ]
        var data = google.visualization.arrayToDataTable(values);

        var options = {
            chart: {
                title: 'Paysips by issuer',
                subtitle: '',
            }
        };

        var chart = new google.charts.Bar(document.getElementById('barchart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
}

async function getauthdata() {
    var dappid = localStorage.getItem("dappid");

    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/authorizers/statistics",
            type: 'post',
   
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            // $('#errormsg').text(error.responseText);
            // $('#ErrorModal').modal('show');
        }
    }
}
async function getissuerdata(){
    var dappid = localStorage.getItem("dappid");
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/issuers/statistics",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            // $('#errormsg').text(error.responseText);
            // $('#ErrorModal').modal('show');
        }
    }
    
}