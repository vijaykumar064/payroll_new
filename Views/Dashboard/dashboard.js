var role = localStorage.getItem("roleId");
var company = localStorage.getItem("companyname");
var email = localStorage.getItem("email");
var belriumtoken = localStorage.getItem("belToken");
var dappid = localStorage.getItem("dappid");
var address = localStorage.getItem("address");
var bel = localStorage.getItem("bel");
 var dep = localStorage.getItem("issuerdepartments");
var iid=localStorage.getItem("issuerid");
var aid=localStorage.getItem("authid");
var rps_offset=0,rps_limit=5,tps_offset=0,tps_limit=5,ips_offset=0,ips_limit=5,pi_limit=5,pi_offset=0,pps_limit=5,pps_offset=0,tem_limit=5,tem_offset=0;
var empselecteddes="",empselecteddep="";
async function model() {
    console.log(role);
    document.getElementById('address').innerText = address;
    $("#balance").text(bel + " " + "BEL");
    $("#balance1").text(bel + " " + "BEL");
    if ((role === "superuser") || (role === "new user")) {
        var list = document.getElementById('heading_list');     
        list.childNodes[3].style.display = "block";
        list.childNodes[7].style.display = "block";        
    }
    if (role === "issuer") {
        var list = document.getElementById('heading_list');
        list.childNodes[1].style.display = "block";
        list.childNodes[7].style.display = "block";
        document.getElementById("issue").style.display = "block";
        document.getElementById("issuerdepartments").innerText=dep;
    }
    if (role === "authorizer") {
        var list = document.getElementById('heading_list');
        list.childNodes[5].style.display = "block";
        list.childNodes[7].style.display = "block";   
    }
    if(role==="superuser"){
        var list = document.getElementById('heading_list');
        list.childNodes[9].style.display = "block";
    }
}

async function issueDashboard() {
    // if(role==="issuer"){
    //     // document.getElementById("category").innerText=category;
    // }
    document.getElementById("username").innerText = email;
    document.getElementById("role").innerHTML = role.toUpperCase();
    if (role != "new user") {
        if(role==="superuser"){
            const response=await getsuperuserdata();
        var titles=["Total Payslips Issued","Total Employee","Total Issuers","Total Authorizers"]
         var superuserdata=[response.payslipsCount,response.employeesCount,response.issuersCount,response.authorizersCount];
        r=document.getElementById('dashboardinfo')
        var fun=["totalpayslips()","totalemployees()","totalissuers()","totalauthorizers()"];
        var add_div="";
       for(i=0;i<4;i++){
        add_div+='<div class="col-xl-3 col-lg-6 col-md-6 col-12" id="naidu"><div class="col-12 dash_details"><div class="col-xl-8 col-lg-8 col-md-8 col-8 dash_details_view"><h3>'+superuserdata[i]+'</h3><a href="#" onclick="'+fun[i]+'">'+titles[i]+'</a></div><div class="col-xl-4 col-lg-4 col-md-4 col-4 dash_details_view_img"><img src="../../lib/img/certificatebig2.png"></div></div></div>'
       }
       r.innerHTML=add_div
     }
 
        if(role==="issuer"){
            const response=await getissuerdata();
            if(response.isSuccess===true){
        var titles=["Pending Payslips","Pending Issues","Total Payslips Issued","Total Employee"]
         var issuerdata=[response.pendingCount,response.authorizedCount,response.issuedCount,response.employeesRegistered];
        r=document.getElementById('dashboardinfo');
        var hoverstyle=["dash_details","dash_details","dash_details","dash_details"]
        var fun=["pendingpayslips()","pendingissues()","totalIssuerpayslips()","totalemployee()"];
        var add_div="";
       for(i=0;i<4;i++){
        add_div+='<div class="col-xl-3 col-lg-6 col-md-6 col-12" id="naidu"><div class="col-12 '+hoverstyle[i]+'"><div class="col-xl-8 col-lg-8 col-md-8 col-8 dash_details_view"><h3>'+issuerdata[i]+'</h3><a href="#" onclick="'+fun[i]+'">'+titles[i]+'</a></div><div class="col-xl-4 col-lg-4 col-md-4 col-4 dash_details_view_img"><img src="../../lib/img/certificatebig2.png"></div></div></div>'
       }
       r.innerHTML=add_div;
        }
    }
 
        if(role==="authorizer"){
            const response=await getauthdata();
            console.log(response);
            if(response.isSuccess===true){
            var titles=["Total Payslips Issued","Pending Approvals","Payslips Rejected","Payslips Approved"]
             var authdata=[response.totalIssued,response.pendingCount,response.rejectedCount,response.signCount];
            r=document.getElementById('dashboardinfo')
            var fun=["totalpayslips()","pendingsigns()","payslipsrejected()","payslipsapproved()"];
            var add_div="";
           for(i=0;i<4;i++){
            add_div+='<div class="col-xl-3 col-lg-6 col-md-6 col-12" id="naidu"><div class="col-12 dash_details"><div class="col-xl-8 col-lg-8 col-md-8 col-8 dash_details_view"><h3>'+authdata[i]+'</h3><a href="#" onclick="'+fun[i]+'">'+titles[i]+'</a></div><div class="col-xl-4 col-lg-4 col-md-4 col-4 dash_details_view_img"><img src="../../lib/img/certificatebig2.png"></div></div></div>'
           }
           r.innerHTML=add_div;
        }

        }
        document.getElementById("company").innerText = company;
        displayRecentPayslips();
}

 async function displayRecentPayslips()
 {
    var resRecentIssued = await recentIssued(rps_limit,rps_offset);
    // alert(JSON.stringify(resRecentIssued));
    var i,paycycle,data=[];
        document.getElementById('dashboard_heading').innerText="Recent Payslip issued";
        document.getElementById('recentsec').style.display="block"; 
        document.getElementById('totempsec').style.display="none";
        document.getElementById('totalsec').style.display="none";
        document.getElementById('pendingsec').style.display="none";
        document.getElementById('signedsec').style.display="none";
        document.getElementById('issuerpayslipsec').style.display="none";

        document.getElementById('recentdynHeader').innerHTML="";
        add_header='<tr><th></th><th>Payslip ID</th><th>Issuer ID</th><th>Employee Name</th><th>Month & Year</th><th></th></tr>';
        document.getElementById('recentdynHeader').innerHTML+=add_header;   
        // if(resRecentIssued.isSuccess===true){  

        for ( i=0 ; i<resRecentIssued.length ; i++ ) {
            paycycle=resRecentIssued[i].month+', '+resRecentIssued[i].year;
            // alert(paycycle);
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayPayslipdata(` + resRecentIssued[i].pid + `)">`+resRecentIssued[i].pid+`</button>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayIssuerData(` + resRecentIssued[i].iid + `)">`+resRecentIssued[i].iid+`</button><br><span>`+resRecentIssued[i].issuedBy+`</span>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(\'` + resRecentIssued[i].empid + `\')">`+resRecentIssued[i].empid+`</button><br><span>`+resRecentIssued[i].name+`</span>`,paycycle,`<a onclick="displayBlockchaindata(\'`+resRecentIssued[i].pid+`\')" href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>`] );            
        // alert(data);
        }
        console.log(data);
        $('#recentDynTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            //scrollY:        200,
            //scrollCollapse: true,
            //scroller:       true,    
            "columns": [
                { "orderable": false },
                null,
                null,
                null,
                null
              ],
            destroy: true,
            drawCallback: function(){
                if(rps_offset!=0)
                {
                    $('#recentDynTable_previous').removeClass('disabled');
                }
                if((rps_limit+rps_offset)<resRecentIssued.total)
                {
                    $('#recentDynTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    rps_offset+=rps_limit;
                    await displayRecentPayslips();
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      rps_offset-=rps_limit;
                      await displayRecentPayslips();
                   });    
                      
             }
        } );
    }
    // }
}


function totalemployees(){
    window.location.href="../Members/members.html";
}

function totalissuers(){
    window.location.href="../SuperAdmin/superadmin-issuers.html?issuer";
//     var querystring=decodeURIComponent(window.location.search);
//    querystring=querystring.substring(1);

}

function totalauthorizers(){
    window.location.href="../SuperAdmin/superadmin-issuers.html?authorizer";

}

async function getsuperuserdata(){
   
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "superuser/statistics",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}




async function getissuerdata(){
    var today = new Date();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (mm < 10) {
       mm = '0' + mm;
    }
    month = mm
    year = yyyy
    var params={
        iid:iid,
        month: month,
        year:year
    }
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "issuer/statistic",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

async function getauthdata(){
    var params={
        aid:aid
    }
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "authorizer/statistic",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}


async function recentIssued(limit,offset) {
    let result;
    // alert(limit+offset);
    var inputs={limit: limit,offset: offset};
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "recentIssued",
            type: 'post',
            data: JSON.stringify(inputs),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(result);
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

// function add_issuedemployees(pid,iid,issuedBy,name,empid,month,year) {
//     add_div = ' <tr><td class="table_prppic"><a href=""><img src="../../lib/img/profile.jpg" width="40"></a></td><td onclick="displayPayslipdata(' + pid + ')" class="table_issued"><a href="#">'+pid+'<a></td><td class="table_proname" onclick="displayIssuerData(' + iid + ')"><a href="#">'+ issuedBy + ' <br><span>'+iid+'</span></a></td><td class="table_proname" onclick="displayEmpData(' + empid + ')"><a href="#">' + name + ' <br><span>' + empid + '</span></a></td><td class="table_issued">'+month+', '+year+'</td><td onclick="displayBlockchaindata(' + pid + ')"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>';
//     document.getElementById('recent_div').innerHTML += add_div;
// }

async function getPendingAuthorizationList()
{
    const resPendingAuth=await PendingAuthorizationList();
    console.log("resPendingAuth.issues: "+JSON.stringify(resPendingAuth.issues));
    document.getElementById('dashboard_heading').innerText="Pending Authorizations";
    document.getElementById('recent_div').innerHTML="";
    add_header='<tr><th></th><th>Employee</th><th>Pay Cycle</th><th>Authorization status</th></tr>';
    document.getElementById('recent_div').innerHTML+=add_header;
    if(resPendingAuth.isSuccess===true)
    {
        for (var i=0;i<resPendingAuth.issues.length;i++)
        {
            addPendingAuthorization(resPendingAuth.issues[i].empid,resPendingAuth.issues[i].employeeEmail,resPendingAuth.issues[i].month,resPendingAuth.issues[i].year,resPendingAuth.issues[i].count,resPendingAuth.issues[i].authCount);
        }
    }
}

function addPendingAuthorization(empid,employeeEmail,month,year,count,authCount)
{
    add_div = '<tr><td class="table_prppic"><a href=""><img src="../../lib/img/profile.jpg" width="40"></a></td><td class="table_proname" onclick="displayEmpData(' + empid + ')"><a href="#">' + employeeEmail + ' <br><span>' + empid + '</span></a></td><td class="table_issued">'+month+', '+year+'</td><td class="table_issued">'+count+'/'+authCount+'</td></tr> ';
    document.getElementById('recent_div').innerHTML += add_div;
}

async function PendingAuthorizationList()
{
    var result;
    params={
        limit: "10", 
        offset: "0"
    }
    try{
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "payslip/initiated",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

//Razor pay code for recharge wallet
function recharge() {
    var options = {
        "key": "rzp_test_gUZDy2ThhDous7",
        "amount": "200000", // 2000 paise = INR 20
        "name": "Belfrics",
        "description": "Dapp Register",
        "image": "bellogo.png",
        "handler": function (response) {
            console.log(response.razorpay_payment_id);
        },
        "prefill": {
            "name": "Vinodh Kumar",
            "email": "vinodh.p@belfricsbt.com"
        },
        "notes": {
            "address": "Yo!Chick"
        },
        "theme": {
            "color": "#528ff0"
        }
    };
    var rzp1 = new Razorpay(options);
    if (role === "new user") {
        rzp1.open();
        event.preventDefault();
    }
    else {
        document.getElementById('recharge1').onclick = function (e) {
            rzp1.open();
            e.preventDefault();
        }
    }
}

async function totalpayslips(tps_limit,tps_offset){
    var result=await totalissues(tps_limit,tps_offset);
    console.log(result);
    // result=
    // {
    //     "issues": [
    //         {
    //             "pid": "2",
    //             "empid": "JohnBonda",
    //             "month": "11",
    //             "year": "2018",
    //             "empname": "JohnBonda"
    //         },
    //         {
    //             "pid": "3",
    //             "empid": "JohnBonda",
    //             "month": "12",
    //             "year": "2018",
    //             "empname": "JohnBonda"
    //         },
    //         {
    //             "pid": "4",
    //             "empid": "JohnBonda",
    //             "month": "01",
    //             "year": "2019",
    //             "empname": "JohnBonda"
    //         },
    //         {
    //             "pid": "5",
    //             "empid": "JohnBonda",
    //             "month": "02",
    //             "year": "2019",
    //             "empname": "JohnBonda"
    //         }
    //     ],
    //     "total": 4,
    //     "isSuccess": true,
    //     "success": true
    // };
    if (result.isSuccess === true) {
        var i,paycycle,data=[];
        document.getElementById('dashboard_heading').innerText="Total Payslips Issued";
        document.getElementById('recentsec').style.display="none";
        document.getElementById('totempsec').style.display="none";
        document.getElementById('totalsec').style.display="block";
        document.getElementById('pendingsec').style.display="none";
        document.getElementById('signedsec').style.display="none";
        document.getElementById('issuerpayslipsec').style.display="none";

        document.getElementById('totaldynHeader').innerHTML="";
        add_header='<tr><th></th><th>Payslip ID</th><th>Issuer ID</th><th>Employee Name</th><th>Month & Year</th><th></th></tr>';
        document.getElementById('totaldynHeader').innerHTML+=add_header;
        for ( i=0 ; i<result.issues.length ; i++ ) {
            paycycle=result.issues[i].month+', '+result.issues[i].year;
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayPayslipdata(` + result.issues[i].pid + `)">`+result.issues[i].pid+`</button>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayIssuerData(` + result.issues[i].iid + `)">`+result.issues[i].iid+`</button><br><span>`+result.issues[i].issuerEmail+`</span>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(` + result.issues[i].empid + `)">`+result.issues[i].empid+`</button><br><span>`+result.issues[i].employeeEmail+`</span>`,paycycle,`<a onclick="displayBlockchaindata(`+result.issues[i].pid+`)" href="#"><img src="https://static.thenounproject.com/png/7467-200.png" style="width:30px;"></a>`] );            
        }
        console.log(data);
        $('#totalDynTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            // scrollY:        200,
            // scrollCollapse: true,
            // scroller:       true,
            destroy: true,
            drawCallback: function(){            
                if(rps_offset!=0)
                {
                    $('#totalDynTable_previous').removeClass('disabled');
                }
                if((rps_limit+rps_offset)<result.total)
                {
                    $('#totalDynTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    rps_offset+=rps_limit;
                    await totalpayslips(rps_limit,rps_offset);
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      rps_offset-=rps_limit;
                      await totalpayslips(rps_limit,rps_offset);
                   });    
                      
             }
        } );        
    }
}

async function totalissues(limit,offset) {
    var params={
        limit: limit,
         offset: offset
    }
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/payslip/issued",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

// function add_totalissues(pid,iid,issuerEmail,empid,email,category,month,year) {
//     add_div = '<tr><td class="table_prppic"><a href=""><img src="../../lib/img/profile.jpg" width="40"></a></td><td onclick="displayPayslipdata(' + pid + ')" class="table_issued"><a href="#">'+pid+'<a></td><td class="table_proname" onclick="displayIssuerData(' + iid + ')"><a href="#">'+ issuerEmail + ' <br><span>'+iid+'</span></a></td><td class="table_proname" onclick="displayEmpData(' + empid + ')"><a href="#">' + email + ' <br><span>' + empid + '</span></a></td><td class="table_issued">'+category+'</td><td class="table_issued">'+month+', '+year+'</td><td onclick="displayBlockchaindata(' + pid + ')"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr> ';
//     document.getElementById('recent_div').innerHTML += add_div;
// }

async function displayIssuerData(iid)
{
    const issuerDetails = await getissuerdetails(iid);
    if(issuerDetails.isSuccess===true)
    {
    try{
        document.getElementById("id_iid").value = issuerDetails.issuer.iid;
        document.getElementById("id_pk").value = issuerDetails.issuer.publickey;
        document.getElementById("id_email").value =issuerDetails.issuer.email;
        document.getElementById("id_designation").value =issuerDetails.issuer.departments;
        document.getElementById("id_empcnt").value = issuerDetails.employeesRegistered;
        document.getElementById("id_issuecnt").value = issuerDetails.issuesCount;
        }
        catch(error){
          console.log(error);
          $('#errormsg').text(error.responseText);
          $('#ErrorModal').modal('show');
        }
        $('#ShowIssuerDetailPop').modal('show');
    }
}

async function getissuerdetails(id){
    let result;
    var inputs = { iid:String(id) };
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/" + "issuer/data",
        type: 'post',
        data: JSON.stringify(inputs),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
     return result;
     console.log(result);
    }
    catch (error) {
        console.log("error");
        if (error) {
          console.log(error);
          $('#errormsg').text(error.responseText);
          $('#ErrorModal').modal('show');
        } 
    }
}

async function displayBlockchaindata(pid)
{
    const resBlockchainData=await getBlockchaindata(pid);
    try
    {
        document.getElementById("ps_issuedon").value = resBlockchainData.issue.timestampp;
        document.getElementById("ps_issuermail").value = resBlockchainData.issuer.email;
        document.getElementById("ps_issuerpk").value = resBlockchainData.issuer.publickey;
        document.getElementById("ps_totauth").value = "3";
        document.getElementById("ps_signauth").value = resBlockchainData.signedAuthorizersCount;
        var auth_table = document.getElementById("authTable");
        $("#authTable").find("tr:not(:first)").remove();
        for(var i in resBlockchainData.signatures){
          var authRow=document.createElement("tr");
          var authCol1=document.createElement("td");
          var myDate = new Date(Number(resBlockchainData.signatures[i].timestampp));
          var time = myDate.toGMTString()
          var authorizedOn=document.createTextNode(time);
          authCol1.appendChild(authorizedOn);
          var authCol2=document.createElement("td");
          var authorizerEmail=document.createTextNode(resBlockchainData.signatures[i].email);
          authCol2.appendChild(authorizerEmail);
          var authCol3=document.createElement("td");
          var authorizerPk=document.createTextNode(resBlockchainData.signatures[i].publickey);
          authCol3.appendChild(authorizerPk);
          authRow.appendChild(authCol1);
          authRow.appendChild(authCol2);
          authRow.appendChild(authCol3);
          auth_table.appendChild(authRow);
        }
    }
    catch(error){
        console.log(error);recent_div
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
    }
        $('#ShowblockchainPop').modal('show');
}

async function getBlockchaindata(pid)
{
    let result;
    var inputs = { pid:String(pid) };
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/payslip/statistic",
        type: 'post',
        data: JSON.stringify(inputs),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
     return result;
     console.log(result);
    }
    catch (error) {
        console.log("error");
        if (error) {
          console.log(error);
          $('#errormsg').text(error.responseText);
          $('#ErrorModal').modal('show');
        } 
    }

}
async function displayEmpData(empid)
{
    const EmployeeDetails = await getEmployeeDetails(empid);
    if(EmployeeDetails.isSuccess===true)
    {
    var employeedetails_table = document.getElementById("identityTable");
    try{
        document.getElementById("ed_eid").value = EmployeeDetails.employee.empid;
        document.getElementById("ed_email").value = EmployeeDetails.employee.email;
        document.getElementById("ed_name").value = EmployeeDetails.employee.name;
        document.getElementById("ed_designation").value = EmployeeDetails.employee.designation;
        document.getElementById("ed_bank").value = EmployeeDetails.employee.bank;
        document.getElementById("ed_acctnum").value = EmployeeDetails.employee.accountNumber;
        document.getElementById("ed_salary").value = EmployeeDetails.employee.salary;
        document.getElementById("ed_wallet").value = EmployeeDetails.employee.walletAddress;
        document.getElementById("ed_category").value = EmployeeDetails.employee.department;
        // while(employeedetails_table.childElementCount > 1){
        //     employeedetails_table.children[1].remove();
        // }
        $("#identityTable").find("tr:not(:first)").remove();
        for(var i in EmployeeDetails.employee.identity){
            var ideRow=document.createElement("tr");
            var ideCol1=document.createElement("td");
            var name=document.createTextNode(i);
            ideCol1.appendChild(name);
            var ideCol2=document.createElement("td");
            var ideVal=document.createTextNode(EmployeeDetails.employee.identity[i]);
            ideCol2.appendChild(ideVal);
            ideRow.appendChild(ideCol1);
            ideRow.appendChild(ideCol2);
            employeedetails_table.appendChild(ideRow);
          }
        }
        catch(error){
          console.log(error);
          $('#errormsg').text(error.responseText);
          $('#ErrorModal').modal('show');
    }
    $('#ShowEmpDetailsPop').modal('show');
  }
}

async function getEmployeeDetails(empid){
    let result;
    var inputs = { empid:String(empid) };
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/" + "employeeData",
        type: 'post',
        data: JSON.stringify(inputs),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
     return result;
     console.log(result);
    }
    catch (error) {
        console.log("error");
        if (error) {
          console.log(error);
          $('#errormsg').text(error.responseText);
          $('#ErrorModal').modal('show');
        } 
    }
}

async function displayPayslipdata(pid) {
    clickedpid=pid;
    pid1 = pid;
    const payslipData = await getPayslipData(pid);
    console.log(payslipData);
    console.log(payslipData.result.name);
    paycycle = payslipData.result.month + ", " + payslipData.result.year;
    
    try{
    document.getElementById("ps_empname").value = payslipData.result.name;
    document.getElementById("ps_pay").value = payslipData.result.accountNumber;
    document.getElementById("ps_designation").value = payslipData.result.designation;
    document.getElementById("ps_idnum").value = payslipData.result.empid;
    document.getElementById("ps_paycycle").value = paycycle;
    document.getElementById("ps_bankdetails").value = payslipData.result.bank;
    document.getElementById("netsalary").textContent = payslipData.result.netSalary;
    const value = inWords(payslipData.result.netSalary).toUpperCase();
    console.log(value);
    document.getElementById("words").textContent = value;
    var id_table = document.getElementById("idTable");
    var earnings_table = document.getElementById("earningsTable");
    var deductions_table = document.getElementById("deductionsTable");
    var otherearnings_table=document.getElementById("otherearningsTable");
    var otherdeductions_table=document.getElementById("otherdeductionsTable");
    $("#idTable").find("tr:not(:first)").remove();
    $("#earningsTable").find("tr:not(:first)").remove();
    $("#deductionsTable").find("tr:not(:first)").remove();
    $("#otherearningsTable").find("tr:not(:first)").remove();
    $("#otherdeductionsTable").find("tr:not(:first)").remove();

    for(var i in payslipData.result.identity){
      var idRow=document.createElement("tr");
      var idCol1=document.createElement("td");
      var name=document.createTextNode(i);
      idCol1.appendChild(name);
      var idCol2=document.createElement("td");
      var idval=document.createTextNode(payslipData.result.identity[i]);
      idCol2.appendChild(idval);
      idRow.appendChild(idCol1);
      idRow.appendChild(idCol2);
      id_table.appendChild(idRow);
    }
    // for(var i in payslipData.result.earnings){
        var earnRow=document.createElement("tr");
        var earnCol1=document.createElement("td");
        var name=document.createTextNode('Basic Salary');
        earnCol1.appendChild(name);
        var earnCol2=document.createElement("td");
        var earnVal=document.createTextNode(payslipData.result.grossSalary);
        earnCol2.appendChild(earnVal);
        earnRow.appendChild(earnCol1);
        earnRow.appendChild(earnCol2);
        earnings_table.appendChild(earnRow);
    //   }
    for(var i in payslipData.result.earnings){
      var earnRow=document.createElement("tr");
      var earnCol1=document.createElement("td");
      var name=document.createTextNode(i);
      earnCol1.appendChild(name);
      var earnCol2=document.createElement("td");
      var earnVal=document.createTextNode(payslipData.result.earnings[i]);
      earnCol2.appendChild(earnVal);
      earnRow.appendChild(earnCol1);
      earnRow.appendChild(earnCol2);
      earnings_table.appendChild(earnRow);
    }
    for(var i in payslipData.result.deductions){
      var dedRow=document.createElement("tr");
      var dedCol1=document.createElement("td");
      var name=document.createTextNode(i);
      dedCol1.appendChild(name);
      var dedCol2=document.createElement("td");
      var dedVal=document.createTextNode(payslipData.result.deductions[i]);
      dedCol2.appendChild(dedVal);
      dedRow.appendChild(dedCol1);
      dedRow.appendChild(dedCol2);
      deductions_table.appendChild(dedRow);
    }
    for(var i in payslipData.result.otherEarnings){
        var dedRow=document.createElement("tr");
        var dedCol1=document.createElement("td");
        var name=document.createTextNode(i);
        dedCol1.appendChild(name);
        var dedCol2=document.createElement("td");
        var dedVal=document.createTextNode(payslipData.result.otherEarnings[i]);
        dedCol2.appendChild(dedVal);
        dedRow.appendChild(dedCol1);
        dedRow.appendChild(dedCol2);
        otherearnings_table.appendChild(dedRow);
      }
      for(var i in payslipData.result.otherDeductions){
        var dedRow=document.createElement("tr");
        var dedCol1=document.createElement("td");
        var name=document.createTextNode(i);
        dedCol1.appendChild(name);
        var dedCol2=document.createElement("td");
        var dedVal=document.createTextNode(payslipData.result.otherDeductions[i]);
        dedCol2.appendChild(dedVal);
        dedRow.appendChild(dedCol1);
        dedRow.appendChild(dedCol2);
        otherdeductions_table.appendChild(dedRow);
      }
    }
    catch(error){
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
    $('#ShowPayslipPop').modal('show');
  }
  
  function inWords(num) {
    var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
  }
  
  async function getPayslipData(pid) {
    let result;
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/" + "payslip/getPayslip",
        type: 'post',
        data: '{"pid":"' + pid + '"}',
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
      return result;
    }
    catch (error) {
      if (error) {
        console.log(error);
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
      }
    }
  }



  async function totalIssuerpayslips(ips_limit,ips_offset){
    var result=await Issuerpayslips(ips_limit,ips_offset);
    console.log(result);
    if (result.isSuccess === true) {
        var i,paycycle,data=[];
        document.getElementById('dashboard_heading').innerText="Total Payslips Issued";
        document.getElementById('recentsec').style.display="none";
        document.getElementById('totempsec').style.display="none";
        document.getElementById('totalsec').style.display="none";
        document.getElementById('issuerpayslipsec').style.display="block";
        document.getElementById('pendingsec').style.display="none";
        document.getElementById('signedsec').style.display="none";
        document.getElementById('issuerpayslipdynHeader').innerHTML="";
        add_header='<tr><th></th><th>Payslip ID</th><th>Employee Name</th><th>Month & Year</th><th></th></tr>';
        document.getElementById('issuerpayslipdynHeader').innerHTML+=add_header;
        for ( i=0 ; i<result.issues.length ; i++ ) {
            paycycle=result.issues[i].month+', '+result.issues[i].year;
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayPayslipdata(` + result.issues[i].pid + `)">`+result.issues[i].pid+`</button>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(` + result.issues[i].empid + `)">`+result.issues[i].empid+`</button><br><span>`+result.issues[i].empname+`</span>`,paycycle,`<a onclick="displayBlockchaindata(`+result.issues[i].pid+`)" href="#"><img src="https://static.thenounproject.com/png/7467-200.png" style="width:30px;"></a>`] );            
        }
        console.log(data);
        $('#issuerpayslipDynTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            // scrollY:        200,
            // scrollCollapse: true,
            // scroller:       true,
            destroy: true,
            drawCallback: function(){            
                if(ips_offset!=0)
                {
                    $('#issuerpayslipDynTable_previous').removeClass('disabled');
                }
                if((ips_limit+ips_offset)<result.total)
                {
                    $('#issuerpayslipDynTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    ips_offset+=ips_limit;
                    await totalIssuerpayslips(ips_limit,ips_offset);
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      ips_offset-=ips_limit;
                      await totalIssuerpayslips(ips_limit,ips_offset);
                   });    
                      
             }
        } );        
    }
}


  async function Issuerpayslips(limit,offset) {
  // getting the data
    let result;
    var inputs = { "iid":iid ,"limit":limit,"offset":offset};
    try{
      result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "issuer/data/issuedPayslips",
      type: 'post',
      data: JSON.stringify(inputs),
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'  });
      return result;
      }

    catch(error)
      {
        console.log(error);
      }
    // console.log(result);
    // document.getElementById('dashboard_heading').innerText="Total Payslips issued";
    // document.getElementById('recent_div').innerHTML="";
    // add_header='<tr><th></th><th>Payslip ID</th><th>Employee</th><th>Paycycle</th><th>Department</th></tr>';
    // document.getElementById('recent_div').innerHTML+=add_header;
    // for (i = 0; i < result['issues'].length; i++) {
    //     add_div = ' <tr><td class="table_prppic"><a href=""><img src="../../lib/img/profile.jpg" width="40"></a></td><td onclick="displayPayslipdata(' + result['issues'][i].pid + ')" class="table_issued"><a href="#">'+result['issues'][i].pid+'<a></td><td class="table_proname" onclick="displayEmpData(' + result['issues'][i].empid + ')"><a href="#">' + result['issues'][i].empname + ' <br><span>' + result['issues'][i].empid + '</span></a></td><td class="table_issued">'+result['issues'][i].month+', '+result['issues'][i].year+'</td><td onclick="displayBlockchaindata(' + result['issues'][i].pid + ')"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>';
    //     document.getElementById('recent_div').innerHTML += add_div;
    // }
  }

  async function pendingpayslips() {
    // getting the data
    let result;
    var data=[];
    var today = new Date();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (mm < 10) {
       mm = '0' + mm;
    }
    month = mm
    year = yyyy
      var inputs = { iid:iid ,month:month,year:year,limit:pps_limit,offset:pps_offset};
      try{
        result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/issuer/pendingIssues",
        type: 'post',
        data: JSON.stringify(inputs),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'  });
      console.log(result);
    
      document.getElementById('dashboard_heading').innerText="Pending Payslips";
      document.getElementById('recentsec').style.display="none";
      document.getElementById('totempsec').style.display="none";
      document.getElementById('totalsec').style.display="none";
      document.getElementById('pendingsec').style.display="block";
      document.getElementById('issuerpayslipsec').style.display="none";

      document.getElementById('signedsec').style.display="none";
      document.getElementById('pendingdynHeader').innerHTML="";
      add_header='<tr><th></th><th>Employee</th><th>Email</th><th>Designation</th><th></th></tr>';
        document.getElementById('pendingdynHeader').innerHTML+=add_header;        
        for (i = 0; i < result['pendingIssues'].length; i++) {
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(\'` + result['pendingIssues'][i].empid + `\')">`+result['pendingIssues'][i].empid+`</button><br><span>` + result['pendingIssues'][i].name+ `</span>`,result['pendingIssues'][i].email,result['pendingIssues'][i].designation,`<button class="initiatebtn" onclick="initiatepayslip(\'`+result['pendingIssues'][i].empid+`\')">initiate</button>`]);            
        }
        console.log(data);
        $('#pendingDynTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            //scrollY:        200,
            //scrollCollapse: true,
            //scroller:       true,
            destroy: true,
            drawCallback: function(){            
                if(pps_offset!=0)
                {
                    $('#pendingDynTable_previous').removeClass('disabled');
                }
                if((pps_limit+pps_offset)<result.total)
                {
                    $('#pendingDynTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    pps_offset+=pps_limit;
                    await pendingpayslips();
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      pps_offset-=pps_limit;
                      await pendingpayslips();
                   });    
                      
             }
        } );
    }
    catch(error)
      {
        console.log(error);
      }
    }

    async function pendingissues() {
        // getting the data
        let result;
          var inputs = { "iid":iid, "limit": pi_limit, "offset": pi_offset };
          var data=[];
          try{
            result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/payslip/confirmedIssues",
            type: 'post',
            data: JSON.stringify(inputs),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'  });
    //         result=
    //         {
    //             "total": 1,
    // "confirmedPayslips": [
    //     {
    //         "pid": "2",
    //         "email": "vinodhp@yopmail.com",
    //         "empid": "JohnBonda",
    //         "name": "John Bonda",
    //         "employer": "Belfrics",
    //         "month": "04",
    //         "year": "2019",
    //         "designation": "SDE",
    //         "bank": "State Bank of India",
    //         "accountNumber": "fowienwfo23392892g",
    //         "identity": "eyJBZGhhciBDYXJkIjoiMzUyOTgyOTM4NTIifQ==",
    //         "earnings": "eyJTYWxhcnkiOiIxMDAwMCIsIkJvbnVzIjoiMzIzNDIifQ==",
    //         "deductions": "eyJMZWF2ZXMgUGVuYWx0eSI6IjUwMCIsIkRhbWFnZSBGZWUiOiIzMjMifQ==",
    //         "grossSalary": "234522",
    //         "totalDeductions": "234",
    //         "netSalary": "235234",
    //         "timestampp": "1549796032373",
    //         "deleted": "0"
    //     }
    // ],
    // "success": true
    //         };
          console.log(result);
          document.getElementById('dashboard_heading').innerText="Waiting for Final Issue";
          document.getElementById('recentsec').style.display="none";
          document.getElementById('totempsec').style.display="none";
          document.getElementById('totalsec').style.display="none";
          document.getElementById('pendingsec').style.display="none";
          document.getElementById('signedsec').style.display="block";
        document.getElementById('issuerpayslipsec').style.display="none";

          document.getElementById('signeddynHeader').innerHTML="";
        add_header='<tr><th></th><th>Payslip ID</th><th>Employee</th><th>Month & Year</th><th></th><th></th></tr>';
        document.getElementById('signeddynHeader').innerHTML+=add_header;        
        for ( i=0 ; i<result['confirmedPayslips'].length; i++ ) {
            paycycle=result['confirmedPayslips'][i].month+', '+result['confirmedPayslips'][i].year;
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayPayslipdata(` + result['confirmedPayslips'][i].pid + `)">`+result['confirmedPayslips'][i].pid+`</button>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(\'` + result['confirmedPayslips'][i].empid + `\')">`+result['confirmedPayslips'][i].empid+`</button><br><span>`+result['confirmedPayslips'][i].name+`</span>`,paycycle,`<button class="issuebtn" onclick="payslipfinalissue(`+result['confirmedPayslips'][i].pid+`)">issue</button>`,`<a onclick="displayBlockchaindata(`+result['confirmedPayslips'][i].pid+`)" href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>`] );            
        }
        console.log(data);
        $('#signedDynTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            //scrollY:        200,
            //scrollCollapse: true,
            //scroller:       true,
            destroy: true,
            drawCallback: function(){        
                if(pi_offset!=0)
                {
                    $('#signedDynTable_previous').removeClass('disabled');
                }
                if((pi_limit+pi_offset)<result.total)
                {
                    $('#signedDynTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    pi_offset+=pi_limit;
                    await pendingissues();
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      pi_offset-=pi_limit;
                      await pendingissues();
                   });    
                      
             }
        });
    }
    catch(error)
      {
        console.log(error);
      }
}

var pid;

function payslipfinalissue(data){
    console.log(data);
    $('#welcome1').modal('show');
    pid=data;
}
        async function totalemployee() {            
            // getting the data
            let result;           
            var data=[];
              var inputs = { iid:iid, limit: tem_limit, offset: tem_offset,department:empselecteddep,designation:empselecteddes };
              try{
                result = await $.ajax({
                url: HOST_URL + "/" + dappid + "/issuer/employeesRegistered",
                type: 'post',
                data: JSON.stringify(inputs),
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json' 
                });
              console.log(result);
              document.getElementById('dashboard_heading').innerText="Registered Employees";
              document.getElementById('recentsec').style.display="none";
          document.getElementById('totempsec').style.display="block";
          document.getElementById('totalsec').style.display="none";
          document.getElementById('pendingsec').style.display="none";
          document.getElementById('signedsec').style.display="none";
        document.getElementById('issuerpayslipsec').style.display="none";

              document.getElementById('dynHeader').innerHTML="";
      add_header='<tr><th></th><th>Employee</th><th>Email</th><th>Designation <div class="dropdown"><img src="../../lib/img/filter.png" class="dropbtn" width="20" height="20"><div class="dropdown-content" id="empdesfilter"></div></div></th><th>Department <div class="dropdown"><img src="../../lib/img/filter.png" class="dropbtn" width="20" height="20"><div class="dropdown-content" id="empdepfilter"></div> </div></th></tr>';
       addempdesdepfilter();
        document.getElementById('dynHeader').innerHTML+=add_header;        
        for (i = 0; i < result['employeesRegistered'].length; i++) {
            data.push( [ `<img src="../../lib/img/profile.jpg" style="border-radius: 50%;" width="40"></img>`,`<button type="button" class="btn btn-default btn-outline-info" onclick="displayEmpData(\'` + result['employeesRegistered'][i].empid + `\')">`+result['employeesRegistered'][i].empid+`</button><br><span>` + result['employeesRegistered'][i].name + `</span>`,result['employeesRegistered'][i].email,result['employeesRegistered'][i].designation,result['employeesRegistered'][i].department] );            
        }
        console.log(data);
        $('#dynamicTable').DataTable( {
            data:           data,
            pagingType: "simple",
            deferRender:    true,
            //scrollY:        200,
            //scrollCollapse: true,
            //scroller:       true,
            destroy: true,
            drawCallback: function(){
                if(tem_offset!=0)
                {
                    $('#dynamicTable_previous').removeClass('disabled');
                }
                if((tem_limit+tem_offset)<result.total)
                {
                    $('#dynamicTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    tem_offset+=tem_limit;
                    await totalemployee();
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      tem_offset-=tem_limit;
                      await totalemployee();
                   });        
             }
        });
        }
        catch(error)
        {
            console.log(error);
        }
            //   document.getElementById('recent_div').innerHTML="";
            //   add_header='<tr><th></th><th>Employee</th><th>Email</th><th>Designation</th><th>Department</th></tr>';
            //   document.getElementById('recent_div').innerHTML+=add_header;
            //   for (i = 0; i < result['employeesRegistered'].length; i++) {
            //       add_div = '<tr><td class="table_prppic"><a href=""><img src="../../lib/img/profile.jpg" width="40"></a></td><td class="table_proname" onclick="displayEmpData(' + result['employeesRegistered'][i].empid + ')"><a href="#">' + result['employeesRegistered'][i].name + ' <br><span>' + result['employeesRegistered'][i].empid + '</span></a></td><td class="table_issued">'+result['employeesRegistered'][i].email+'</td><td class="table_issued">'+result['employeesRegistered'][i].designation+'</td><td class="table_issued">'+result['employeesRegistered'][i].department+'</td><td><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></td></tr>';
            //       document.getElementById('recent_div').innerHTML += add_div;
            //   }
    }
        
            function pendingsigns()
            {
                window.location.href='../Authorizer/authorizer.html?pendingsigns';
            }
            function payslipsrejected()
            {
                window.location.href='../Authorizer/authorizer.html?payslipsrejected';
            }
            function payslipsapproved()
            {
                window.location.href='../Authorizer/authorizer.html?payslipsapproved';
            }

            function totalissuers()
            {
                window.location.href='../SuperAdmin/superadmin-issuers.html?totalissuers';
            }
            function totalauthorizers()
            {
                window.location.href='../SuperAdmin/superadmin-issuers.html?totalauthorizers';
            }
            function initiatepayslip(data){
                console.log(data);
                window.location.href='../Certificate/certificate.html?initiate='+data+'';
            }





          async  function addempdesdepfilter(){
              const desresponse=await getdesList();
              console.log(desresponse);
              if(desresponse.isSuccess===true){
                if(desresponse.designation.length>0){
                  var add_div="";
              for(i=0;i<desresponse.designation.length;i++){
                add_div+='<a onclick="getempselecteddes(\''+desresponse.designation[i]+'\')" href="#">'+desresponse.designation[i]+'</a>'
              }
              var empty="none";
              add_div+='<a onclick="getempselecteddes(\''+empty+'\')" href="#">none</a>'
               document.getElementById("empdesfilter").innerHTML=add_div;
              }
            }
            const depresponse=await getdepartments();
            console.log(depresponse);
            if(depresponse.isSuccess===true){
                if(depresponse.departments.length>0){
                  var add_div="";
              for(i=0;i<depresponse.departments.length;i++){
                add_div+='<a onclick="getempselecteddep(\''+depresponse.departments[i].name+'\')" href="#">'+depresponse.departments[i].name+'</a>'
              }
              var empty="none";
              add_div+='<a onclick="getempselecteddep(\''+empty+'\')" href="#">none</a>'
               document.getElementById("empdepfilter").innerHTML=add_div;
            }
        }
        }




function getempselecteddes(data){
    console.log(data);
    if(data=="none"){
        empselecteddes="";
    }
    else{
        empselecteddes=data;
    }
    totalemployee();
}


function getempselecteddep(data){
    console.log(data);
    if(data=="none"){
        empselecteddep="";
    }
    else{
        empselecteddep=data;
    }
    totalemployee();
}

async function getdesList()
{
    console.log("in des list")
    let result;
    try {
       result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/employees/getDesignations",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        // console.log(JSON.stringify(result)+"sdrtfgyuhji");
        return result;
       
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}
  

async function getdepartments(){
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/getDepartments/authorizers",
            type: 'post',
            data:{},
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
  }
  




  async function finalIssue() {    
    var flag=await finalpassphrase();
    if(flag===true){
    var payslip = {
        "pid": pid,
        "fee": "0",
        "iid": iid,
        "secret": document.getElementById('passphrase1').value,
        "dappid": dappid
    };
    let result;
    console.log(payslip);
    $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="font-size:70px; color: #00bfff"></i>', css: { backgroundColor: 'transparent', border: '0' } });
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/issueTransactionCall",
            type: 'post',
            data: JSON.stringify(payslip),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(result);
        if (result.isSuccess == false) {
            $('#errormsg').text("Failed to issue");
            $('#ErrorModal').modal('show');
            return;
        }
        else{
            $('#errormsg').text("Successfully Issued Payslip");
            $('#ErrorModal').modal('show');
        }
        if (result.transactionId != undefined) {    
            document.getElementById(selectedEmpid + "status").value = 'Issued';
            document.getElementById(selectedEmpid + 'status').setAttribute('id', '#popup2');
            // document.getElementById('tagclose4').click();
        }

    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
    $.unblockUI();
    //setTimeout(function(){$.unblockUI()},12000);
}
}