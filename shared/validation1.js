//homevalidations
function homeValidation() {
    if (homeemail()) {
        return true;
    }
    return false;
}
function homeemail() {
    var email = document.getElementById('email');
    if (email.value != "") {
        var res = emailValidation(email, "* Please enter a valid email address *", "p1")
        if (res) {
            email.style.borderColor = ''
            return true;
        }
    }
    else {
        document.getElementById('p1').innerText = "* field mandatory *";
        email.style.borderColor = 'red'
        return false;
    }
}


//login page validations
async function loginValidation() {
    var err = true;
    var id = document.getElementById("Password")
    var len=id.value
    console.log(len.length)
    if (len.length == 0) {
        document.getElementById('p2').innerText = "* field mandatory *";
        id.style.borderColor='red'
        err = false;
    }
    if (!loginemail()) {
        err = false;
    }
    if (!loginpassword()) {
        err = false;
    }
    console.log(err);
    return err;
}

function loginemail() {
    var email = document.getElementById('emailid');
    if (email.value != "") {
        var res = emailValidation(email, "* Please enter a valid email address *", "p1")
        if (res) {
            email.style.borderColor = ''
            return true;
        }
    }
    else {
        document.getElementById('p1').innerText = "* field mandatory *";
        email.style.borderColor = 'red'
        return false;
    }
}

function loginpassword() {
    var password = document.getElementById('Password');
    if (password.value.length > 0) {
        var res = passwordValidate(password, "* Password must be of 7-14 length consisting of at least 1 capital letter , 1 number and 1 special character *", "p2");
        if (res) {
            password.style.borderColor='';
            return true;
        }
    }
}

function passwordValidate(inputtext, alertMsg, id) {
    var validpassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,14}$/;
    if (inputtext.value.match(validpassword)) {
        document.getElementById(id).innerText = "";
        return true;
    }
    else {
        document.getElementById(id).innerText = alertMsg;
        inputtext.style.borderColor='red';
       // inputtext.focus();
        return false;
    }
}


//signup page validations

function signupValidation() {
    var err = true
    var password = document.getElementById('Password');
    if (password.value.length == 0) {
        document.getElementById('p5').innerText = "* field mandatory *";
        // password.focus();
        err = false;
    }
    if (!validateCompany()) {
        err = false;
    }
    if (!signupvalidatename()) {
        err = false;
    }
    if (!signupvalidateEmail()) {
        err = false;
    }
    if (!signupcountry()) {
        err = false;
    }
    if (!signupvalidatePassword()) {
        err = false;
    }
    if (!validateCpassword()) {
        err = false;
    }
    return err;
}
function validateCompany() {
    var CompanyName = document.getElementById('companyname');
    if (CompanyName.value != "") {
        var res = inputAlphabet(CompanyName, "* please use alphabets only *", "p1");
        console.log("error");
        if (res) {
            CompanyName.style.borderColor = ''
            return true;
        }
    }
    else {
        document.getElementById('p1').innerText = "* field mandatory *";
        CompanyName.style.borderColor = 'red'
        return false;
    }
}
function signupvalidatename() {
    var UserName = document.getElementById('username');
    if (UserName.value != "") {
        var res = inputAlphabet(UserName, "* please use alphabets only *", "p2");
        if (res) {
            UserName.style.borderColor=''
            return true;
        }
    }
    else {
        document.getElementById('p2').innerText = "* field mandatory *";
        UserName.style.borderColor = 'red'
        return false;
    }

}

function signupvalidateEmail() {
    var email = document.getElementById('email');
    if (email.value != "") {
        var res = emailValidation(email, "Please enter a valid email address", "p3");
        if (res) {
            email.style.borderColor = ''
            return true;
        }
    }
    else {
        document.getElementById('p3').innerText = "* field mandatory *";
        email.style.borderColor = 'red'

        return false;
    }
}
function signupcountry() {
    console.log("hai");
    document.getElementById('p4').innerText = " ";
    var e = document.getElementById("country_list");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser == 0) //for text use if(strUser1=="Select")
    {
        document.getElementById('p4').innerText = "* field mandatory *";
        e.style.borderColor = 'red'
        return false;
    }
    else {
        e.style.borderColor=''
        return true;
    }
}


function signupvalidatePassword() {
    var password = document.getElementById('Password');
    if (password.value != "")
        var res = passwordValidate(password, "* Password must be of 7-14 length consisting of at least 1 capital letter , 1 number and 1 special character *", "p5");
    if (res) {
        password.style.borderColor=''
        return true;
    }
    else {
        password.style.borderColor = 'red'
        return false;
    }
}

function validateCpassword() {
    var password = document.getElementById('Password');
    var confirmpassword = document.getElementById('confirmpassword');
    if (confirmpassword.value != "")
        var res = passwordMatch(password, confirmpassword, "* password and cpassword doesn't match *", "p6");
    if (res) {
        confirmpassword.style.borderColor=''
        return true;
    }
    else {
        confirmpassword.style.borderColor = 'red'
        return false;
    }
}


function passwordMatch(inputtext1, inputtext2, alertMsg, id) {
    if (inputtext1.value === inputtext2.value) {
        document.getElementById(id).innerText = "";
        inputtext1.style.borderColor=''
        return true;
    }
    else {
        document.getElementById(id).innerText = alertMsg;
       // inputtext2.focus();
       inputtext1.style.borderColor = 'red'

        return false;
    }
}

//wallet page update dapp bal validation

function updatedappvalidate() {
    var err = true;
    if (!walletvalidation()) {
        err = false;
    }
    if (!amountvalidation()) {
        err = false;
    }
    if (!passphrasevalidation()) {
        err = false;
    }
    return err;
}


function walletvalidation() {
    var wallet = document.getElementById('walletaddress');
    console.log(wallet.length)
    if (wallet.value != "") {
        var res = walletaddressValidation(wallet, "* Please enter a valid Wallet address *", "b1")
        if (res) {
            wallet.style.borderColor = ''
            return true;
        }
    }
    else {
        var a = document.getElementById('b1');
        a.innerText = "* Field Mandatory *";
        wallet.style.borderColor = 'red'
        return false;
    }
}

function walletaddressValidation(inputtext, alertMsg, id) {
    var validwallet = /^[a-zA-z0-9 ]{36}$/;
    if (inputtext.value.match(validwallet)) {
        document.getElementById(id).innerText = "";
        inputtext.style.borderColor = '';
        return true;
    }
    else {
        document.getElementById(id).innerText = alertMsg;
       // inputtext.focus();
        inputtext.style.borderColor = 'red';
        return false;
    }
}

function amountvalidation() {
    var amount = document.getElementById('amount');
    console.log(amount.length)
    if (amount.value != "") {
        // var res=integervalidate(amount, "Please enter a valid Wallet amount","b2")

        document.getElementById('b2').innerText = " ";
        amount.style.borderColor = '';
        return true;
    }
    else {
        var a = document.getElementById('b2')
        a.innerText = "* Field Mandatory *";
        amount.style.borderColor = 'red'
        return false;
    }
}


function passphrasevalidation() {
    var passphrase = document.getElementById('passphrase');
    console.log(passphrase.length)
    if (passphrase.value != "") {
        var res = inputAlphabet(passphrase, "Please enter a valid passphrase", "b3")
        //   document.getElementById('b3').innerText =" ";
        if (res) {
            passphrase.style.borderColor = ''
            return true;
        }
    }
    else {
        document.getElementById('b3').innerText = "* Field Mandatory *";
        passphrase.style.borderColor = 'red'
        return false;
    }
}


//add issuer form validation
function issuerformvalidation() {
    var err = true;
    // if (!categoryvalidate1()) {
    //     err = false;
    // }
    if (!countryvalidate1()) {
        err = false;
    }
    if (!issuername1()) {
        err = false;
    }
    // if (!issuerdes1()) {
    //     err = false;
    // }
    if (!issueremail1()) {
        err = false;
    }
    return err;
}
function categoryvalidate1() {
    console.log("categoryvalidation");
    document.getElementById('c1').innerText = " ";
    var e = document.getElementById("category1");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser === "select department") //for text use if(strUser1=="Select")
    {
        document.getElementById('c1').innerText = "* field mandatory *";
        e.style.borderColor='red'
        return false;
    }
    else {
        e.style.borderColor='';
        return true;
    }

}
function countryvalidate1() {
    console.log("hai");
    document.getElementById('p1').innerText = " ";
    var e = document.getElementById("countrycode");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser == 0) //for text use if(strUser1=="Select")
    {
        document.getElementById('p1').innerText = "* field mandatory *";
        e.style.borderColor='red';
        return false;
    }
    else {
        e.style.borderColor='';
        return true;
    }
}
function issuername1() {
    var name = document.getElementById('issuername');
    if (name.value != "") {
        var res = inputAlphabet(name, "* For your name please use alphabets only *", "p2");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('p2').innerText = "* field mandatory *";
        name.style.borderColor='red'
        return false;
    }
}
function issuerdes1() {
    var designation = document.getElementById('designation');
    if (designation.value != "") {
        var res = inputAlphabet(designation, "* For your designation please use alphabets only *", "p3");
        if (res)
        designation.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('p3').innerText = "* field mandatory *";
        designation.style.borderColor='red'
        return false;
    }
}

function issueremail1() {
    var email = document.getElementById('email');
    if (email.value != "") {
        var res = emailValidation(email, "* Please enter a valid email address *", "p4")
        if (res) {
            email.style.borderColor=''
            return true;
        }
    }
    else {
        document.getElementById('p4').innerText = " * field mandatory *";
        email.style.borderColor='red'
        return false;
    }
}
//string validation
function inputAlphabet(inputtext, alertMsg, id) {
    var alphaExp = /^[a-zA-Z ]+$/;
    if (inputtext.value.match(alphaExp)) {
        console.log("match");
        document.getElementById(id).innerText = " ";
        inputtext.style.borderColor=''
        return true;
    }
    else {
        console.log("doestmatch");
        console.log(alertMsg);
        document.getElementById(id).innerText = alertMsg;
        input.style.borderColor='red'
        // inputtext.focus();
        return false;
    }
}
//email validation
function emailValidation(inputtext, alertMsg, id) {
    var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (inputtext.value.match(emailExp)) {
        document.getElementById(id).innerText = " ";
        inputtext.style.borderColor=''
        return true;
    } else {
        document.getElementById(id).innerText = alertMsg; //this segment displays the validation rule for email
        //inputtext.focus();
        input.text.borderColor='red'
        return false;
    }
}


//add authorizer form validation
function authorizerformvalidation() {
    var err = true;
    // if (!categoryvalidate2()) {
    //     err = false;
    // }
    if (!countryvalidate2()) {
        err = false;
    }
    if (!authname1()) {
        err = false;
    }
    // if (!authdes1()) {
    //     err = false;
    // }
    if (!authemail1()) {
        err = false;
    }
    return err;
}

function categoryvalidate2() {
    console.log("categoryvalidation");
    document.getElementById('c2').innerText = " ";
    var e = document.getElementById("category2");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser === "select category") //for text use if(strUser1=="Select")
    {
        document.getElementById('c2').innerText = "* field mandatory *";
        e.style.borderColor='red'
        return false;
    }
    else {
        e.style.borderColor=''
        return true;
    }

}
function countryvalidate2() {
    console.log("hai");
    document.getElementById('p5').innerText = " ";
    var e = document.getElementById("countrycode1");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser == 0) //for text use if(strUser1=="Select")
    {
        document.getElementById('p5').innerText = "* field mandatory *";
        e.style.borderColor='red'
        return false;
    }
    else {
        e.style.borderColor=''
        return true;
    }
}
function authname1() {
    var name = document.getElementById('name1');
    if (name.value != "") {
        var res = inputAlphabet(name, "* For your name please use alphabets only *", "p6");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('p6').innerText = "* field mandatory *";
        name.style.borderColor='red'
        return false;
    }
}
function authdes1() {
    var designation = document.getElementById('designation1');
    if (designation.value != "") {
        var res = inputAlphabet(designation, "* For your designation please use alphabets only *", "p7");
        if (res)
        designation.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('p7').innerText = "* field mandatory *";
        designation.style.borderColor='red'
        return false;
    }
}

function authemail1() {
    var email = document.getElementById('email1');
    if (email.value != "") {
        var res = emailValidation(email, "* Please enter a valid email address *", "p8")
        if (res) {
            email.style.borderColor=''
            return true;
        }
    }
    else {
        document.getElementById('p8').innerText = " * field mandatory *";
        email.style.borderColor='red'
        return false;
    }
}






// function integervalidate(inputtext, alertMsg,id){
//     var validnumber=/^[0-9]{1-9}$/;
//       if(inputtext.value.match(validnumber)){
//           document.getElementById(id).innerText = "";
//           return true;  
//       }
//       else{
//           document.getElementById(id).innerText = alertMsg;
//           inputtext.focus();
//           return false;
//       }

// }


//members page employee registration validations
function empregformValidation() {
    var err = true;
    if (!empcountryvalidate1()) {
        err = false;
    }
    if (!empregvalidateEmail1()) {
        err = false;
    }
    if (!validateName1()) {
        err = false;
    }
    if (!validateLname1()) {
        err = false;
    }
    if (!validateDes1()) {
        err = false;
    }
    if (!validateBank1()) {
        err = false;
    }
    if (!validateAcc1()) {
        err = false;
    }
    if (!validatePan1()) {
        err = false;
    }
    if (!validateSalary1()) {
        err = false;
    }
    if (!employeeid1()) {
        err = false;
    }
    if(!validateDep1()){
        err=false;
    }
    return err;
}
function empcountryvalidate1() {
    document.getElementById('r1').innerText = "";
    var e = document.getElementById("countrycode");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser == "Country Code") //for text use if(strUser1=="Select")
    {
        document.getElementById('r1').innerText = "* field mandatory *";
        e.style.borderColor='red';
        document.getElementById('email').disabled=true;
        return false;
    }
    else {
        e.style.borderColor='';   
        document.getElementById('email').focus();
        document.getElementById('email').disabled=false;
        return true;
    }
}

function empregvalidateEmail1() {
    var email = document.getElementById('email');
    if (email.value != "") {
        var res = emailValidation(email, "Please enter a valid email address", "r2")
        if (res) {
        email.style.borderColor='';
        document.getElementById('fname').focus();
        document.getElementById('fname').disabled=false;
            return true;
        }
    }
    else {
        document.getElementById('r2').innerText = "* field mandatory *";
        email.style.borderColor='red';
        document.getElementById('fname').disabled=true;
        return false;
    }
}
function validateLname1() {
    var lastName = document.getElementById('lname');
    if (lastName.value != "") {
        var res = valstring(lastName, "For your name please use alphabets only", "r4");
        if (res)
        {
            lastName.style.borderColor='';
            document.getElementById('empid1').focus();
            document.getElementById('empid1').disabled=false;
            return true;
        }
    }
    else {
        document.getElementById('r4').innerText = "* field mandatory *";
        lastName.style.borderColor='red';
        document.getElementById('empid1').disabled=true;
        return false;
    }
}

function validateName1() {
    var name = document.getElementById('fname');
    if (name.value != "") {
        var res = valstring(name, "For your name please use alphabets only", "r3");
        if (res)
        name.style.borderColor='';
        document.getElementById('lname').focus();
        document.getElementById('lname').disabled=false;
            return true;
    }
    else {
        document.getElementById('r3').innerText = "* field mandatory *";
        name.style.borderColor='red';
        document.getElementById('lname').disabled=true;
        return false;
    }
}

function employeeid1() {
    var id = document.getElementById('empid1');
    if (id.value != "") {
        var res = validateid(id, "Enter valid id", "r5");
        if (res)
        {
            id.style.borderColor='';   
            document.getElementById('pannum').focus();
            document.getElementById('pannum').disabled=false;
            return true;
        }
    }
    else {
        id.style.borderColor='red'
        document.getElementById('r5').innerText = "* field mandatory *";
        document.getElementById('pannum').disabled=true;
        return false;
    }

}


function validatePan1() {
    var pan = document.getElementById('pannum');
    if (pan.value != "") {
        // var res = valPan(pan, "pan format not correct", "r6");
        // if (res)
        document.getElementById('r6').innerText = "";
        pan.style.borderColor='';
        document.getElementById('salary1').focus();
        document.getElementById('salary1').disabled=false;
        return true;
    }
    else {
        document.getElementById('r6').innerText = "* field mandatory *";
        pan.style.borderColor='red';
        document.getElementById('salary1').disabled=true;
        return false;
    }
}

function validateSalary1() {
    var salary = document.getElementById('salary1');
    if (salary.value != "") {
        var res = valSal(salary, "salary format incorrect", "r7");
        if (res)
        {
            salary.style.borderColor='';
            document.getElementById('department').focus();
            document.getElementById('department').disabled=false;
            return true;
        }
    }
    else {
        document.getElementById('r7').innerText = "* field mandatory *";
        salary.style.borderColor='red';
        document.getElementById('department').disabled=true;
        return false;
    }
}

function validateDes1() {
    var e = document.getElementById("designation1");
    var des=e.options[e.selectedIndex].text;
   
    if (des != "Designation") {
        // var res = valstring(designation, "only alphabets", "r8");
        // if (res)
        document.getElementById('r8').innerText = " ";

        e.style.borderColor='';
        document.getElementById('rbank').focus();
        document.getElementById('rbank').disabled=false;
            return true;
    }
    else {
        document.getElementById('r8').innerText = "* field mandatory *";
        e.style.borderColor='red';
        document.getElementById('rbank').disabled=true;
        return false;
    }
}

function validateBank1() {
    document.getElementById('r9').innerText = "";
    var e = document.getElementById("rbank");
    var strUser = e.options[e.selectedIndex].value;
    console.log(strUser);
    //if you need text to be compared then use
    if (strUser == "Bank") //for text use if(strUser1=="Select")
    {
        document.getElementById('r9').innerText = "* field mandatory *";
        e.style.borderColor='red';
        document.getElementById('regButton').disabled=true;
        return false;
    }
    else {
        e.style.borderColor='';
        document.getElementById('regButton').focus();
        document.getElementById('regButton').disabled=false;
        return true;
    }
}

function validateAcc1() {
    var accountNumber = document.getElementById('accnum');
    if (accountNumber.value != "") {
        var res = valAccNo(accountNumber, "please provide valid acc no", "r10");
        if (res)
        {
        accountNumber.style.borderColor='';
        document.getElementById('designation1').focus();
        document.getElementById('designation1').disabled=false;
            return true;
        }
    }
    else {
        document.getElementById('r10').innerText = "* field mandatory *";
        accountNumber.style.borderColor='red';
        document.getElementById('regButton').disabled=true;
        return false;
    }
}
function valstring(text, alertMsg, id) {
    var letters = /^[A-Za-z ]+$/;
    if (text.value.match(letters)) {
        document.getElementById(id).innerText = "";
        text.style.borderColor=''
        return true;
    }
    else {
        document.getElementById(id).innerText = alertMsg;
        text.style.borderColor='red'
        // text.focus();
        return false;
    }
}


  //Salary :
  function valSal(sal, alertMsg,id)
  {
    var salary = /^[0-9]+$/;
    if(sal.value.match(salary))
          {   
            document.getElementById(id).innerText = "";
            sal.style.borderColor=''
            return true;
          }
        else
          {
            document.getElementById(id).innerText = alertMsg;
            sal.style.borderColor='red'
            //sal.focus();
            return false;
          }
  }


  //Account number :
  function valAccNo(num, alertMsg,id)
  {
    var accno = /^[0-9]{15,18}$/;
    if(num.value.match(accno))
          {
            document.getElementById(id).innerText = "";
            num.style.borderColor=''
            return true;
          }
        else
          {
            document.getElementById(id).innerText = alertMsg;   
            num.style.borderColor='red'
           // num.focus();
            return false;
          }
  }


    //pan card:  
    function valPan(pan, alertMsg,id)
    {
      var pattern = /^([a-zA-Z]{5})([0-9]{4})([a-zA-Z]{1})$/;
      if(pan.value.match(pattern))
      { 
        document.getElementById(id).innerText = "";  
        pan.style.borderColor=''
        return true;
      }
    else
      { 
        document.getElementById(id).innerText = alertMsg;
        pan.style.borderColor='red'
        // pan.focus();
        return false; 
      }
      }


      function validateid(input, alertMsg,id){
          console.log(input.value)
        var pattern = /^[a-zA-Z0-9]+$/;
        if(input.value.match(pattern))
        { 
            console.log("corect")
          document.getElementById(id).innerText = "";  
          input.style.borderColor=''
          return true;
        }
      else
        { 
            console.log("wrong")
          document.getElementById(id).innerText = alertMsg;
          input.style.borderColor='red'
          //input.focus();
          return false; 
        }
      }


      function lengthvalidate(){
        var id = document.getElementById('empid');
        if (id.value != "") {
            document.getElementById('l1').innerText = "";

            id.style.borderColor=''
                return true;
        }
        else {
            document.getElementById('l1').innerText = "* field mandatory *";
            id.style.borderColor='red'
            return false;
        }
      }


      function monthvalidate(){
        var id = document.getElementById('MonthYear');
        if (id.value != "") {
            document.getElementById('d1').innerText = "";
            id.style.borderColor=''
                return true;
        }
        else {
            document.getElementById('d1').innerText = "* field mandatory *";
            id.style.borderColor='red'
            return false;
        } 
      }

      function passphraselength(){
        var id = document.getElementById('passphrase');
        if (id.value != "") {
            document.getElementById('s1').innerText = "";
            id.style.borderColor=''
                return true;
        }
        else {
            document.getElementById('s1').innerText = "* field mandatory *";
            id.style.borderColor='red'
            return false;
        } 


      }
 

      function finalpassphrase(){
        var id = document.getElementById('passphrase1');
        if (id.value != "") {
            document.getElementById('f1').innerText = "";
            id.style.borderColor=''
                return true;
        }
        else {
            document.getElementById('f1').innerText = "* field mandatory *";
            id.style.borderColor='red'
            return false;
        } 

      }



    //   dapp Register validation in wallet page

    function dappvalidation(){
     var err=true;
     if(!passphrasevalidate()){
         err=false;
     }
     if(!dappnamevalidate()){
         err=false;
     }
     if(!dappdescvalidate()){
         err=false;
     }
     if(!dappcompanyvalidate()){
         err=false
     }
     return err;
        }

function passphrasevalidate(){
    var name = document.getElementById('passphrase1');
    if (name.value != "") {
        var res = valstring(name, "* use alphabets only *", "n1");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('n1').innerText = "* field mandatory *";
        name.style.borderColor='red';
        return false;
    }


}

function dappnamevalidate(){
    var name = document.getElementById('dappname');
    if (name.value != "") {
        var res = valstring(name, "* use alphabets only *", "n2");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('n2').innerText = "* field mandatory *";
        name.style.borderColor='red';
        return false;
    }
}

function dappdescvalidate(){
    
    var name = document.getElementById('description');
    if (name.value != "") {
        var res = valstring(name, "* use alphabets only *", "n3");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('n3').innerText = "* field mandatory *";
        name.style.borderColor='red';
        return false;
    }

}

function dappcompanyvalidate(){
    var name = document.getElementById('company1');
    if (name.value != "") {
        var res = valstring(name, "* use alphabets only *", "n4");
        if (res)
        name.style.borderColor=''
            return true;
    }
    else {
        document.getElementById('n4').innerText = "* field mandatory *";
        name.style.borderColor='red';
        return false;
    }
}



function validateidentity(){
    var id = document.getElementById('Identity');
        if (id.value != "") {
            document.getElementById('i1').innerText = "";
            id.style.borderColor=''
                return true;
        }
        else {
            document.getElementById('i1').innerText = "* field mandatory *";
            id.style.borderColor='red'
            return false;
        } 
  
}

function validateDep1() {    
    var e = document.getElementById("department");
    var dep=e.options[e.selectedIndex].text;
    if (dep != "Department") {
        // var res = valstring(designation, "only alphabets", "r8");
        // if (res)
        document.getElementById('r11').innerText = " "; 
        e.style.borderColor='';
        document.getElementById('accnum').disabled=false;
            return true;
    }
    else {
        document.getElementById('r11').innerText = "* field mandatory *";
        e.style.borderColor='red';
        document.getElementById('accnum').disabled=true;
        return false;
    }
 }


function validatedep(){
    var id = document.getElementById('add_department');
    console.log(id.value);
    if((id.value ==="")||(id.innerText==="Department Name")){
        console.log(id.value)
        document.getElementById('d1').innerText = "* field mandatory *";
        id.style.borderColor='red'
        return false;
    } 
    else{
            document.getElementById('d1').innerText = "";
            id.style.borderColor='';
                return true;
    }
}

function validateFee()
{    
    var fee = document.getElementById('servicefeeval');    
    if(fee.value!='')
    {
        if(!isNaN(fee.value))
        {
            document.getElementById('invalidFee').innerText = "";
            fee.style.borderColor='';
            document.getElementById('servicefeesavebtn').disabled=false;
            return true;
        }   
        else
        {            
            document.getElementById('invalidFee').innerText = "Enter only numbers";
            fee.style.borderColor='red';
            document.getElementById('servicefeesavebtn').disabled=true;
        }         
    }
    else
    {
        console.log("false");
        document.getElementById('invalidFee').innerText = "* Field mandatory";
        fee.style.borderColor='red';
        document.getElementById('servicefeesavebtn').disabled=true;
        return false;
    }
}