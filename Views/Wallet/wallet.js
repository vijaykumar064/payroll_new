var role, bel, user, companyname, email, wallet, address, token, dappid, kycStatus,selectedpackage,result,id;
role = localStorage.getItem("roleId");
bel = localStorage.getItem("bel");
email = localStorage.getItem("email");
address = localStorage.getItem("address");
dappid = localStorage.getItem('dappid');
kycStatus = localStorage.getItem("kycStatus");
selectedpackage=localStorage.getItem('package')
var dep = localStorage.getItem("issuerdepartments");

companyname = localStorage.getItem("companyname")
async function model() {
    var querystring=decodeURIComponent(window.location.search);
    if(querystring!=''){ 
        
        // console.log(querystring);
        querystring=querystring.substring(1);
        console.log(querystring);
        var params = querystring.split("=");
        // var token = params[0];
        result= params[1];
        id=params[2];
// alert(id);
const res=await getData();
if(res['isSuccess']){
    localStorage.setItem('package',res['data']['local']['package'])
    localStorage.setItem('address',res['data']['local']['address'])
    localStorage.setItem('bel',res['data']['local']['bel'])
    localStorage.setItem('belToken',res['data']['local']['belToken'])
    localStorage.setItem('kycStatus',res['data']['local']['kycStatus'])
    localStorage.setItem('roleId',res['data']['local']['roleId'])
    localStorage.setItem('secret',res['data']['local']['secret'])
    localStorage.setItem('email',res['data']['local']['email'])
    localStorage.setItem('bkvsdm_token',res['data']['local']['bkvsdm_token'])
    role = localStorage.getItem("roleId");
bel = localStorage.getItem("bel");
email = localStorage.getItem("email");
address = localStorage.getItem("address");
kycStatus = localStorage.getItem("kycStatus");
selectedpackage=localStorage.getItem('package')
}
      

    }
    document.getElementById("wallet").innerText = (bel + " " + "BEL");
    document.getElementById("address").value = address;
    document.getElementById("name").innerText = email;
    document.getElementById("role").innerHTML = role.toUpperCase();
    document.getElementById("balance").innerText = (bel + " " + "BEL");
    console.log("kycStatus: " + kycStatus);
    console.log(role);
    if (role === 'new user') {
        ul = document.getElementById('dappbalance');
        if (kycStatus === 'false') {
            a = ul.childNodes[3].getElementsByTagName('a');
            console.log(a);
            a['0']['dataset']['target'] = '';
            // a = ul.childNodes[5].getElementsByTagName('a');
            // a['0']['dataset']['target'] = '';
        }
        else{
            if(querystring!=''){
                console.log(result);
                flag=localStorage.getItem('flag');
                if(result==='success'){
                    // alert(flag);
                    // alert(flag!="true");
                    if(flag!="true"){
                        const res=await sendmoneytouser();
                        if(res['isSuccess'])
                        {
                           localStorage.setItem('flag','true')
                            // $('#infomsg').text("Recharge Successful");
                            document.getElementById('afterrecharge').click();
                        // $('#rechargesuccess').modal('show'); 
                            // document.getElementById('dappreg').click();
                        const response=await savepackage();
                        console.log(response);
                        }
                    }
                    else{
                        document.getElementById('afterrecharge').click();
                        // $('#rechargesuccess').modal('show'); 
                    }
                   
        
                }
            }
            new_user_blur();
            // const res=await checkpackage();
            // console.log(res);
            var show = document.getElementsByClassName('showToUser');
            for (var i = 0; i < show.length; i++) {
                console.log(show[i]);
                if(i!=1)
                show[i].style.display = 'block';
            }  
        }

        atags = document.getElementsByTagName('a');
        atags[atags.length - 1].href = '../Kyc/kyc.html';
        document.getElementById('dappbal').style.display = 'none';
    }
    else if (role !== "new user") {
        document.getElementById("company").innerText = companyname;
        var response = await getDappBalance();
        if (response == undefined) {
            response = '0';
        }
        document.getElementById('dapp1').innerText = response + " BEL";
    }

    if ((role === "superuser") || (role === "new user")) {
        var list = document.getElementById('heading_list');
        list.childNodes[3].style.display = "block";
        list.childNodes[7].style.display = "block";
        // document.getElementById("rechargewallet").style.display = "block";
        // document.getElementById("updateDapp").style.display = "block";
        // if (role === 'new user') {
        //     document.getElementById("regdapp").style.display = "block";
        // }
    }
    if (role === "issuer") {
        var list = document.getElementById('heading_list');
        list.childNodes[1].style.display = "block";
        list.childNodes[7].style.display = "block";
        document.getElementById("issuerdepartments").innerText=dep;
    }
    else if (role === "authorizer") {
        var list = document.getElementById('heading_list');
        list.childNodes[5].style.display = "block";
        list.childNodes[7].style.display = "block";
    }
    if(role==="superuser"){
        var list = document.getElementById('heading_list');
        list.childNodes[9].style.display = "block";
        document.getElementById('renewpackage').style.display='block';
    }

    // if (role === "new user" && kycStatus === "true") {

        // await displayPackageList()
        //pop up terms and conditions
        // $('#terms1').text("1.Recharge your wallet to register a Dapp.");
        // $('#terms2').text("2.For Registering a Dapp you need 50 Bels.");
        // $('#terms3').text("3.Goto settings and add departments and designations for authorizer levels");
        // $('#terms4').text("4.Add payslip fields for employees");
        // $('#terms5').text("5.For issuing a payslip you need to add issuer or Authorizer.");
        // $('#terms6').text("6.For adding an issuer or Authorizer you need atleast 5 Bels in your Dapp.");
        // $('#terms7').text("7.If your Dapp balance is less than 5 Bels update your Dapp Balance");
        // $('#termsModal').modal('show');
    // }
    if (kycStatus === "false") {
        new_user_blur();
        var show = document.getElementsByClassName('showToUser');
        for (var i = 0; i < show.length; i++) {
            if(i!=show.length-1)
            show[i].style.display = 'block';
        }
        $('#term1').text("1.We have created a wallet for you with 2 Bel. ");
        $('#term2').text("2.Your KYC must be verified before you proceed further.");
        // $('#term3').text("3.If you are an Employer you need to Register a Dapp with us.");
        $('#term4').text("4.Click OK to proceed further");
        $('#terms_Modal').modal('show');
    }
    // await displayPackageList()
}

function goDappRegMandatory() {
    console.log(bel);
    // if (bel >= 50) {
        document.getElementById("dappreg").click();
    // }
}

function dappregmodal(){
    document.getElementById('dappregsteps').click();
    $('#terms1').text("1.Recharge your wallet to register a Dapp.");
    $('#terms2').text("2.For Registering a Dapp you need 50 Bels.");
    $('#terms3').text("3.Goto settings and add departments and designations for authorizer levels");
    $('#terms4').text("4.Add payslip fields for employees");
    $('#terms5').text("5.For issuing a payslip you need to add issuer or Authorizer.");
    $('#terms6').text("6.For adding an issuer or Authorizer you need atleast 5 Bels in your Dapp.");
    $('#terms7').text("7.If your Dapp balance is less than 5 Bels update your Dapp Balance");
    $('#termsModal').modal('show');
}
async function getDappBalance() {
    var result;
    try {
        result = await $.ajax({
            type: 'get',
            url: HOST_URL + "/" + dappid + "/" + "accounts/balance",
            data: { "dappId": dappid, "address": address }
        });
        console.log(result);
        return result['account']['balances'][0];
    }
    catch (error) {
        console.log(error);
    }
}

function checkstatus() {
    if (kycStatus === 'true'){
       if(Number(bel) >= 50) {
            try {
                document.getElementById('regdapp').click();
            }
            catch{
                console.log('error');
            }
            return;
        }
        else{
            try {
                document.getElementById('rechargewallet').click();
            }
            catch{
                console.log('error');
            }
            return;
        }
    } 

}

function change() {
    ul = document.getElementById('activity');
    elements = ul.children.length;
    for (var i = 0; i < elements; i++) {
        if (ul.children[i].style.color != 'green') {
            ul.children[i].style.color = 'green';
            break;
        }
    }
}

function buttonshow() {
    if (role === 'new user' && kycStatus == 'true') {
        document.getElementById('skipbutton').style.display = 'block';
    }
} 

function simple() { document.getElementById('setting').style.filter = ''; }

async function registerdapp() {
    var flag=await dappvalidation();
    if(flag===true){
    // token = localStorage.getItem("belToken");
    // alert(kycStatus)
    if(kycStatus == "true")
    {
        var passphrase = document.getElementById("passphrase1").value;
        var dappname = document.getElementById("dappname").value;
        var description = document.getElementById("description").value;
        var company = document.getElementById("company1").value;
        // var country = localStorage.getItem("country");
        var country="India";
        var email = localStorage.getItem("email");

        var params = {
            secret: passphrase,
            des: description,
            email: email,
            company: company,
            country: country,
            name: dappname
        }

        console.log(params);
        $('#reg-dapp').modal('hide');
        $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="margin-left: 20px; font-size:70px; color: #00bfff"></i>', css: { backgroundColor: 'transparent', border: '0' } });
        const res = await dappreg(params);
        console.log(res);
        if (res["isSuccess"] === true) {
            // $('#reg-dapp').modal('hide');
            $('#successmsg').text("Your DAPP got " + res.message+". Please login again");
            $('#dappsuccess').modal('show');
            localStorage.setItem("dappid", res["dappid"]);
            // document.getElementById("close").click();
            // $('#infomsg').text("")
            // $('#InfoModal').modal('show');

        }
        else
        {
            document.getElementById('dapppfailmsg').innerHTML=res.error;
            $('#reg-dapp').modal('show');
            // $('#infomsg').text(res.error);
            // $('#InfoModal').modal('show');
        }
    }
    else {
        $('#infomsg').text("Get your KYC Verified to Register your Dapp");
         $('#InfoModal').modal('show');
    }
    $.unblockUI();
}
}  //end of registerdapp function

function loggingout(){
    document.getElementById("logout").click();
}
//dappreg API call function
async function dappreg(data){
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + SDAPP_ID + "/" + "makeDapp",
            data: JSON.stringify(data),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');

            } else {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');
            }
        }
    }
} // end of dappreg API call function

//Razor pay code for recharge wallet
async function recharge()
{
    $('#recharge-wallet').modal('hide');
    var amount=$('#rechargeamount').val();
    if(amount<0)
    {
        $('#infomsg').text("Please enter a valid amount!");
        $('#InfoModal').modal('show');
    }
    else
    {
        const resCreateOrder = await createOrder(amount);
        var walletAddress=address;        
        countryCode=walletAddress.substring(walletAddress.length-2,walletAddress.length);        
        console.log(JSON.stringify(resCreateOrder));
        var options = 
        {
            "key": "rzp_test_gUZDy2ThhDous7",
            "order_id": resCreateOrder.id,
            "name": "Belfrics",
            "description": "Dapp Register",
            "image": "../../lib/img/belbooks-logo.svg",
            "handler": async function (response) {
                console.log("response: "+JSON.stringify(response));
                const resVerifyAndCapture=await verifyAndCapture(response.razorpay_order_id,response.razorpay_payment_id,response.razorpay_signature,walletAddress,countryCode,String(resCreateOrder.amount));
                if(resVerifyAndCapture.isSuccess===true)
                {
                    var mes=resVerifyAndCapture.message.split(',');
                    $('#infomsg').text(mes[0]);
                    $('#extrainfo').text(mes[1]);
                    $('#InfoModal').modal('show');            
                }
                else
                {            
                    $('#infomsg').text(resVerifyAndCapture.message);
                    $('#InfoModal').modal('show');            
                }
            },
            "prefill": {
            "name": "Vinodh Kumar",
            "email": "vinodh.p@belfricsbt.com"
            },
            "notes": {
            "address": "Yo!Chick"
            },
            "theme": {
            "color": "#528ff0"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }
}

async function createOrder(amount)
{
    let result;
    var input={ "amount": amount };
    try{
        result = await $.ajax({
        url: SERVERURL + "/razorpay/createOrder",
        type: 'post',
        data: JSON.stringify(input),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json' 
     });
        console.log(JSON.stringify(result)); 
        return result;
        } 
    catch(error)
        {
            console.log(error);
        }
}

async function verifyAndCapture(ord_id,payment_id,signature,walletAddress,countryCode,amount)
{
    let result;
    var input=
    {
        "ord_id": ord_id,
        "payment_id": payment_id,
        "signature": signature,
        "address":walletAddress,
        "countryCode":countryCode,
        "amount":amount
    };
    try{
        result = await $.ajax({
        url: SERVERURL + "/rechargeWallet",
        type: 'post',
        data: JSON.stringify(input),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'  });
        console.log(JSON.stringify(result)); 
        return result;
        } 
    catch(error)
        {
            console.log(error);
        }
}

async function dappbal(){
    var flag=await updatedappvalidate();
    console.log(flag)
    if(flag===true){
    const res=await updatedappbal()
    console.log(JSON.stringify(res));
    }
}
async function updatedappbal(){
   var secret=document.getElementById("passphrase").value;
    var amount=document.getElementById("amount").value;
    var countryCode=walletAddress.substring(walletAddress.length-2,walletAddress.length);
   var params={ 
        secret:secret,
        dappId:dappid,
        amount:Number(amount),
        countryCode: countryCode
    }
    let result;
    try {
        result = await $.ajax({
            type: 'put',
            url: 'http://node1.belrium.io/api/dapps/transaction',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');
                console.log(JSON.stringify(result));

            } else {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');
            }
        }
    }
}  //end of Razor pay code for recharge wallet



async function sendmoneytouser(){
   
    console.log(address.length);
    console.log(address.substr(address.length -2))
    countrycode=address.substr(address.length -2);
    var params={
    amount:2,
    countrycode:countrycode,
    address:address
    }
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: 'http://52.201.227.220:8080/onlybel',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');
                console.log(JSON.stringify(result));

            } else {
                $('#infomsg').text("error" + error["responseJSON"]["message"]);
                $('#InfoModal').modal('show');
            }
        }
    }
}


async function checkpackage(){
    var params={
        email:email
        }
        let result;
        try {
            result = await $.ajax({
                type: 'post',
                url: 'http://52.201.227.220:8080/getuser',
                data: JSON.stringify(params),
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json'
            });
            return result;
        }
        catch (error) {
            if (error) {
                console.log(error);
                if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                    $('#infomsg').text("error" + error["responseJSON"]["message"]);
                    $('#InfoModal').modal('show');
                    console.log(JSON.stringify(result));
    
                } else {
                    $('#infomsg').text("error" + error["responseJSON"]["message"]);
                    $('#InfoModal').modal('show');
                }
            }
        }

}

async function savepackage(){
    var params={
        id:selectedpackage,
    email:email
        }
        let result;
        try {
            result = await $.ajax({
                type: 'post',
                url: 'http://52.201.227.220:8080/apply',
                data: JSON.stringify(params),
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json'
            });
            return result;
        }
        catch (error) {
            if (error) {
                console.log(error);
                if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                    $('#infomsg').text("error" + error["responseJSON"]["message"]);
                    $('#InfoModal').modal('show');
                    console.log(JSON.stringify(result));
    
                } else {
                    $('#infomsg').text("error" + error["responseJSON"]["message"]);
                    $('#InfoModal').modal('show');
                }
            }
        }
}


async function getData(){
    // var params={
    //     id:id,
    // }
    let result;
    try {
        result = await $.ajax({
            type: 'get',
            // data:JSON.stringify(params),
            url: 'http://52.201.227.220:8080/getData/'+id,
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                // $('#infomsg').text("error" + error["responseJSON"]["message"]);
                // $('#InfoModal').modal('show');
                console.log(JSON.stringify(error["responseJSON"]["message"]));

            } else {
                // $('#infomsg').text("error" + error["responseJSON"]["message"]);
                // $('#InfoModal').modal('show');
                console.log(JSON.stringify(error["responseJSON"]["message"]));

            }
        }
    }
}