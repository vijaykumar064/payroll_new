function new_user_blur(){
    atags = document.getElementsByTagName('a');
    for(var i=0;i<atags.length;i++){
        atags[i].href='#';
    }
    document.getElementsByTagName('header')[0].style.filter='blur(1px)';
    document.getElementsByClassName('leftmenu')[1].style.filter='blur(1px)';
    document.getElementsByClassName('footer')[0].style.display='none';
    try{
        var div = document.getElementById('blur_it');
        div.style.filter='blur(2px)';
    }catch{
        console.log("");
    }
}


function undo_blur(){
    document.getElementsByTagName('header')[0].style.filter='';
    document.getElementsByClassName('leftmenu')[1].style.filter='';
    document.getElementsByClassName('footer')[0].style.display='block';
}