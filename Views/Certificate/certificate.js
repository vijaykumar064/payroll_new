var role = localStorage.getItem("roleId");
var company = localStorage.getItem("companyname");
var email = localStorage.getItem("email");
var belriumtoken = localStorage.getItem("belToken");
var dappid = localStorage.getItem("dappid");
var bel = localStorage.getItem("bel");
const iid = localStorage.getItem('issuerid');
var countryData, countryCode1;
const count_countries = 253;
var registerData;
var str, date;
var pid = "";
var selectedEmpid = '';
var prev_pid = undefined;
var employeeid = undefined;
var flag = false;
var prev_month = '';
var monthb;
var employeeCertificate;
var prev = '';
let page_issuedCertificates = 1;
var month, year, se_limit=5,se_offset=0, count = 0,ge_limit=10,ge_offset=0;
var earningsobject,deductionsobject,otherearningsobject,otherdeductionsobject;
var identityproof;
var selecteddesfilter="",selectedstatus="";
var dep = localStorage.getItem("issuerdepartments");


const calssButtons = { 'Pending': "pendingbtn", 'Authorized': 'authorizedbtn', 'Issued': 'issuedbtn', 'Initiated': 'initiatedbtn' ,'Rejected':'rejectedbtn'};



function change1() {
    document.getElementById('issue_new_certificate').style.filter = ""
    if (employeeid === undefined) {
        document.getElementById('issue_new_certificate').style.filter = "blur(2px)"
        var div = document.getElementById('press');
        div.click();
    }
    if ((employeeid != undefined) && (flag === true)) {
        document.getElementById("tabclick").click();
        document.getElementById("press1").click();
        document.getElementById('issue_new_certificate').style.filter = "blur(2px)";
    }
}

function tagclose() {    
    employeeid = undefined;
    document.getElementById('tab1').click();
}

async function checkemp() {
    var flag=await lengthvalidate()
    if(flag===true){
    const response = await empexists()
    console.log(response);
    $("#welcome10").modal('hide');    
    if (response.isSuccess === true) {
        document.getElementById("list").click();
        console.log(response.employee)
        var n=response.employee
        
        len = n.length;
        var i;
       console.log("hai"+len)
        document.getElementById('found').innerHTML = "";
        for (i = 0; i < len; i++) {            
            add_foundlist(n[i].empid, n[i].name, n[i].email,n[i].designation,response.status);
        }
    }
    else {                
         document.getElementById("notfound").click();
    }
}
}

function add_foundlist(id, name, email, des,status) {
    console.log(status);
    add_div = '<tr> <td>' + id + '</td><td>' + name + '</td> <td>' + email + '</td><td>' + des + '</td>  <td class="table_viewbtn"> <input type="submit" name="signin" class="sign_btn btn-primary" value="ok" data-dismiss="modal" onclick="takedata(\'' + id + '\'' + ' ,\'' + status + '\'' + ')"></input> </td></tr>'
    document.getElementById('found').innerHTML += add_div;
  }

function takedata(id,status){      
console.log(status);
      console.log(id);
    if(status==="employee"){
        employeeid=Number(id);        
    document.getElementById("press1").click();
    }
    else{
        document.getElementById("pendingemp").click();
    }
  }


function registeruser(){
    document.getElementById("reg1").click();
}

async function empexists() {
    employeeid = document.getElementById("empid").value;
    console.log(employeeid);
    let result1;
    result1 = await $.ajax({
        url: HOST_URL + "/" + dappid + "/employee/id/exists",
        type: 'post',
        data: '{"text":"' + employeeid + '"}',
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
    });
    return result1;
}
function successregister() {
    document.getElementById("tab1").click();
}
async function checkstatus() {
    date = document.getElementById("MonthYear").value;
    var flag=await monthvalidate();
    if(flag===true){
    $("#datepicker").modal('hide');
   console.log(date);
    if (date == '') {
        return;
    }
    var str1 = date.split('-');
    month = str1[0];
    year = str1[1];
    var params = {
        empid: employeeid,
        month: month,
        year: year,
        iid:iid
    }
    console.log(params);
    //console.log(params.employeeid);
    let result1;
    console.log(month + year)
    result1 = await $.ajax({
        url: HOST_URL + "/" + dappid + "/employee/payslip/month/status",
        type: 'post',
        data: JSON.stringify(params),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
    });
    console.log(result1);
    if (result1.isSuccess === true) {
        var status = result1.result.status

        if (status === "Pending") {
            const employee = await $.ajax({
                url: HOST_URL + "/" + dappid + "/employeeData",
                type: 'post',
                data: '{"empid":"' + employeeid + '"}',
                contentType: 'application/json;charset=UTF-8',
                dataType: 'json'
            });
            console.log(status)
            console.log(employee);
            fillDetails(employee);
            employeeid = undefined;
            document.getElementById('issue_new_certificate').style.filter = ""
        }
        if (status === "Initiated") {
            console.log("hai")
            document.getElementById("press2").click();
        }
        if (status === "Authorized") {
            document.getElementById("press3").click();
        }
        if (status === "Issued") {
            console.log("hello")
            document.getElementById("press4").click();
        }
        if(status==="Rejected"){
            document.getElementById("pressme").click();
        }
    }
    else{
        $('#errormsg').text(error.message);
        $('#ErrorModal').modal('show');
    }
}
}


async function model() {
    //document.getElementById("tabclick").click();
    var querystring = decodeURIComponent(window.location.search);
    if (querystring) {
        querystring = querystring.substring(1);
        // alert(querystring);
        console.log(querystring);
        querystring1 = querystring.split("=");
        // alert(querystring1[0] + querystring1[1]);
        if(querystring1[0]==="action"){
            if (querystring1[1] === "'newpayslip'") {
                // console.log("hai");
                document.getElementById("tabclick").click();
            }   
        }
        else if(querystring1[0]==="initiate"){
            // alert(querystring1[1]);
            getStatus('Pending', querystring1[1])
        }
     

    }

    if ((role === "superuser") || (role === "new user")) {
        var list = document.getElementById('heading_list');
        //    list.childNodes[5].remove();
        //    list.childNodes[1].remove();
        list.childNodes[3].style.display = "block";
        list.childNodes[7].style.display = "block";
        //document.getElementById("reg").remove();
        //document.getElementById("reg1").remove();
        //  var list1 = document.getElementById('testing_list');
        //   list1.childNodes[3].remove();

        //  document.getElementById("issuedcerts").remove();
        //   document.getElementById('removeThis').remove();
    }
    if (role === "issuer") {
        var list = document.getElementById('heading_list');
        //    list.childNodes[5].remove();
        //    list.childNodes[3].remove();
        list.childNodes[1].style.display = "block";
        list.childNodes[7].style.display = "block";
        document.getElementById("reg").style.display = "block";
        var list1 = document.getElementById('testing_list');
        list1.childNodes[3].style.display = "block";

    }
    if (role === "authorizer") {
        var list = document.getElementById('heading_list');
        // list.childNodes[3].remove();
        // list.childNodes[1].remove();
        list.childNodes[5].style.display = "block";
        list.childNodes[7].style.display = "block";
        // var list1 = document.getElementById('testing_list');
        // list1.childNodes[5].remove();
        //document.getElementById("reg").remove();
        //document.getElementById("reg1").remove();
    }
    if (role != "new user") {
        document.getElementById("company").innerText = company;
    }
    if(role==="superuser"){
        var list = document.getElementById('heading_list');
        list.childNodes[9].style.display = "block";
    }
    if(role==="issuer"){
        document.getElementById("issuerdepartments").innerText=dep;
        var depdropdown = document.getElementById("department");
            var str=dep.split(',')
            for(i in str) {
                depdropdown.options[depdropdown.options.length] = new Option(str[i], i);
            }
    }
    document.getElementById("username").innerText = email;
    document.getElementById("role").innerHTML = role.toUpperCase();
    document.getElementById("balance").innerText = (bel + " " + "BEL");
    // const deplist=await getdepList();
    // }


    const empdeslist=await getdesList();
    console.log(empdeslist);
    var desdropdown = document.getElementById("designation1");
    if(empdeslist.isSuccess===true)
    {
        for(i in empdeslist.designation) {
            desdropdown.options[desdropdown.options.length] = new Option(empdeslist.designation[i], i);
        }
        desdropdown.options[desdropdown.options.length] = new Option("Other", desdropdown.options.length-2);
    }


    // const desresponse=await getdes();
    // console.log(desresponse);
    if(empdeslist.isSuccess===true){
        if(empdeslist.designation.length>0){
            add_des=""
            for(i=0;i<empdeslist.designation.length;i++){
                // add_des+='<a href="#">designation</a>'
                // console.log(add_des);
                 add_des+='<a onclick="getdesdata(\''+empdeslist.designation[i]+'\')" href="#">'+empdeslist.designation[i]+'</a>'
            }
            var empty="none";
            add_des+='<a onclick="getdesdata(\''+empty+'\')" href="#">none</a>';
            // console.log(add_des);
            document.getElementById("empdesfilter").innerHTML=add_des;
        }
    }

    var status=["Pending","Initiated","Authorized","Issued","Rejected"];
    add_status=""
    for(i=0;i<status.length;i++){
        // add_des+='<a href="#">designation</a>'
        // console.log(add_des);
         add_status+='<a onclick="getstatusfilterdata(\''+status[i]+'\')" href="#">'+status[i]+'</a>'
    }
    var empty="none";
    add_status+='<a onclick="getstatusfilterdata(\''+empty+'\')" href="#">none</a>';
    // console.log(add_des);
    document.getElementById("payslipstatusfilter").innerHTML=add_status;
}



function getdesdata(data){
    console.log(data);
    if(data=="none"){
        selecteddesfilter="";
    }
    else{
        selecteddesfilter=data;
    }
    console.log(month+year+ge_limit+ge_offset+selecteddesfilter+selectedstatus)
    getEmployees(month, year,ge_limit,ge_offset,selecteddesfilter,selectedstatus);
}

function getstatusfilterdata(data){
    console.log(data);
    if(data=="none"){
        selectedstatus="";
    }
    else{
        selectedstatus=data;
    }
    console.log(month+year+ge_limit+ge_offset+selecteddesfilter+selectedstatus)
    getEmployees(month, year,ge_limit,ge_offset,selecteddesfilter,selectedstatus);
}




//Interface call for adding all employees in the table registered by issuer for particular a month and year
function getMont(month1, event) {
    monthb = month1;
    var new_month = Number(month1);
    prev_month= Number(prev_month);

    butts = document.getElementsByClassName('konda');
    butts[prev_month - 1].classList.remove('activebutton');
    prev_month = new_month;
    butts[new_month - 1].classList += " activebutton";
    month = monthb;
}


function loademployee() {
    var today = new Date();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (mm < 10) {
        mm = '0' + mm
    }
    month = mm
    year = yyyy
    prev_month = mm;
    document.getElementsByClassName('konda')[mm - 1].classList += ' activebutton';
    getEmployees(month, year,ge_limit,ge_offset,selecteddesfilter,selectedstatus);
}

function displayemployee() {
    var e = document.getElementById("year1");
    // console.log(e);
    year = e.options[e.selectedIndex].text;
    console.log(year)
    count = 0
    var id = document.getElementById("search").value;
    if (id != "") {
        searchemployee(monthb, year, id,se_limit,se_offset);
    }
    else {
        getEmployees(monthb, year,ge_limit,ge_offset,selecteddesfilter,selectedstatus);
    }

}

function searchemployee(month, year, id,se_limit,se_offset) {
    var tabledata=[];
    // alert(id);
    document.getElementById('showsearchemptable').style.display="block";
    document.getElementById('showemptable').style.display="none";
    $.post(HOST_URL + "/" + dappid + "/" + "employee/payslip/month/status",
        { month: month, year: year, limit: se_limit, offset: se_offset, empid: id },
        function (data) {
            console.log(JSON.stringify(data));
            var data = data.result;        
            var disabled = '';
            var target = '';
            var func = '';
            disabled = '';
            if (role === 'superuser' || role === 'authorizer') {
                disabled = 'disabled';
            }
            if (data.status == 'Initiated') {
                target = ' data-toggle="modal" data-target="#popup" ';
            }
            if (data.status == 'Authorized') {
                target = ' data-toggle="modal" data-target="#welcome1" ';
                if (data.iid != iid) {
                    disabled = 'disabled';
                }
                else {
                    disabled = '';
                }
                func = ",setPid(" + data.pid + "," + data.empid + ")";

            }
            if (data.status == 'Issued') {
                target = ' data-toggle="modal" data-target="#popup2" ';

            }
            if(data.status == 'Rejected'){
                target = ' data-toggle="modal" data-targekeyt="#rejectpopup" ';
                // fun =""
            }
            tabledata.push([data.empid,data.name,data.designation,'<button ' + target + ' ' + disabled + ' onclick="getStatus(' + '\'' + data.status + '\'' + ',\'' + data.empid + '\'' + ')' + func + ' " id=' + data.empid + 'status class ="' + calssButtons[data.status] + '" >' + data.status + ' </button> &nbsp; &nbsp; &nbsp; &nbsp; <a href="#"><span><i  onClick="showtranshash(' + '\'' + data.status + '\'' + ',\'' + data.pid + '\'' + ')" class="fa fa-eye" aria-hidden="true"></i></span></a>']); 
        console.log("table data :"+tabledata);
        $('#searchempTable').DataTable( {
            data:             tabledata,
            pagingType:       "simple",
            deferRender:      true,
            //scrollY:        200,
            //scrollCollapse: true,
            //scroller:       true,
            destroy: true,
            drawCallback: function(){
                if(se_offset!=0)
                {
                    // alert(se_offset);
                    $('#searchempTable_previous').removeClass('disabled');
                }
                if((se_limit+se_offset)<1)
                {
                    $('#searchempTable_next').removeClass('disabled');
                }
                
                $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                    //alert('next');
                    se_offset+=se_limit;
                    await searchemployee(month, year, id,se_limit,se_offset);
                    
                   });
                   $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                   .on('click', async function(){
                      //alert('previous');
                      se_offset-=se_limit;
                      await searchemployee(month, year, id,se_limit,se_offset);
                   });        
             }
          });
        });
}

async function getEmployees(month, year, ge_limit,ge_offset,selecteddesfilter) {
    document.getElementById('showsearchemptable').style.display="none";
    document.getElementById('showemptable').style.display="block";
    var params={iid:iid, month: month, year: year, limit: ge_limit, offset: ge_offset,designation:selecteddesfilter,status:selectedstatus};
    var data=[];
    if(role!="new user"){
    let result;
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/" + "payslip/month/status",
        type: 'post',
        data: JSON.stringify(params),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
      var response = result.result;        
                var disabled = '';
                for (const key in response) {
                    if (response.hasOwnProperty(key)) {
                        const value = response[key];
                        var target = '';
                        var func = '';
                        disabled = '';
                        if (role === 'superuser' || role === 'authorizer') {
                            disabled = 'disabled';
                        }
                        if (value.status == 'Initiated') {
                            target = ' data-toggle="modal" data-target="#popup" ';
                            console.log(value.pid);
                        }
                        if (value.status == 'Authorized') {
                            target = ' data-toggle="modal" data-target="#welcome1" ';
                            if (value.iid != iid) {
                                disabled = 'disabled';
                            }
                            else {
                                disabled = '';
                            }
                            func = ",setPid(" + value.pid + "," + key + ")";
    
                        }
                        if (value.status == 'Issued') {
                            target = ' data-toggle="modal" data-target="#popup2" ';
    
                        }
                        if(value.status == 'Rejected'){
                            // target = ' data-toggle="modal" data-target="#rejectpopup" ';
                        }
                        data.push([key,value.name,value.designation,'<button ' + target + ' ' + disabled + ' onclick="getStatus(' + '\'' + value.status + '\'' + ',\'' + key + '\'' + ')' + func + ' " id=' + key + 'status class ="' + calssButtons[value.status] + '" >' + value.status + ' </button> &nbsp; &nbsp; &nbsp; &nbsp; <a href="#"><span><i  onClick="showtranshash(' + '\'' + value.status + '\'' + ',\'' + value.pid + '\'' + ')" class="fa fa-eye" aria-hidden="true"></i></span></a>']);                    
                }
            }
            $('#empTable').DataTable( {
                data:             data,
                pagingType:       "simple",
                deferRender:      true,
                //scrollY:        200,
                //scrollCollapse: true,
                //scroller:       true,
                destroy: true,
                drawCallback: function(){
                    if(ge_offset!=0)
                    {
                        $('#empTable_previous').removeClass('disabled');
                    }
                    if((ge_limit+ge_offset)<result.total)
                    {
                        $('#empTable_next').removeClass('disabled');
                    }
                    
                    $('.paginate_button.next:not(.disabled)', this.api().table().container())          
                       .on('click', async function(){
                        //alert('next');
                        ge_offset+=ge_limit;
                        await getEmployees(month, year, ge_limit,ge_offset,selecteddesfilter);
                        
                       });
                       $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
                       .on('click', async function(){
                          //alert('previous');
                          ge_offset-=ge_limit;
                          await getEmployees(month, year, ge_limit,ge_offset,selecteddesfilter);
                       });        
                 }
              });
    }
    catch (error) {
      if (error) {
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
      }
    }
}
}

async function showtranshash(status,pid){
    console.log(status+pid);
   // http://localhost:9305/api/dapps/6f5197bb89daae4b28014eb3236492954472cfe5403b9f20dc7dce96d014a0a9/payslip/statistic
var params={
    pid:pid
}
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "payslip/statistic",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(result);
       // console.log(result.issuer.publickey);
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
    if(status==="Initiated"){
        document.getElementById("initiatedstats").click();
        // document.getElementById("issuerpk").innerText=result.issuer.publickey;
        document.getElementById("totalauths").innerText="3";
        document.getElementById("pendingauths").innerText=3-result.signatures.length;
        document.getElementById("empemail3").innerText=result.payslip.email;
        document.getElementById("issueremail3").innerText=result.issuer.email;
        document.getElementById("status3").innerText="Authorization Pending";
        document.getElementById("authlevel").innerText=result.currentAuthLevel;
        var myDate = new Date(Number(result.payslip.timestampp));
        var time = myDate.toGMTString()
        document.getElementById("datentime3").innerText=time;
    }
    if(status==="Issued"){
        document.getElementById("confirmedstats").click();
       // document.getElementById("hash").innerText=result.
       document.getElementById("signature").innerText=result.issue.sign;
        document.getElementById("publickey").innerText=result.issue.publickey;
        document.getElementById("issuedby").innerText=result.issuer.email;
        document.getElementById("tansid").innerText=result.transaction.id;
        document.getElementById("issuedto").innerText=result.payslip.email;
        // document.getElementById("cat").innerText=result.issue.department;
        document.getElementById("dhash").innerText=result.issue.hash;
        var myDate = new Date(Number(result.issue.timestampp));
        var time = myDate.toGMTString()
        document.getElementById("dnt").innerText=time;
        var names=result.signatures[0].email;
        console.log(names);
        var n=result.signatures.length;
        console.log(n);
        if(n>1){
        for(i=1;i<n;i++){
            
            console.log(i)
            names=names+','+result.signatures[i].email
            console.log(names)
        }
    }
        document.getElementById("authorizedby").innerText=names;
    }
    if(status==="Rejected"){
        document.getElementById("Rejectedstats").click();
        document.getElementById("authorizeremail").innerText=result.rejectedBy.email;
        document.getElementById("issueremail").innerText=result.issuedBy.email;
        var myDate = new Date(Number(result.rejectedBy.timestampp));
        var time = myDate.toGMTString()
        document.getElementById("datentime").innerText=time;
        document.getElementById("rejectedby").innerText=result.rejectedBy.publickey;
        document.getElementById("issuerpublickey").innerText=result.issuedBy.publickey;
        document.getElementById("reasons").innerText=result.reason;
        document.getElementById("category1").innerText=result.issuedBy.department;

        
    }
    if(status==="Authorized"){
        document.getElementById("authstats").click();
        console.log(result.issue.publickey);
        var pk=result.issue.publickey;
        document.getElementById("issuerpk1").innerText=pk;
        document.getElementById("authstatus").innerText="Authorized";
        document.getElementById("finalstatus").innerText="Waiting for issuer sign"; 
       
        var names=result.signatures[0].email;
        console.log(names);
        var n=result.signatures.length;
        console.log(n);
        if(n>1){
        for(i=1;i<n;i++){
            
            console.log(i)
            names=names+','+result.signatures[i].email
            console.log(names)
        }
        document.getElementById("authby").innerText=names;
        document.getElementById("issueremail1").innerText=result.issuer.email;        
        document.getElementById("empemail").innerText=result.payslip.email;        
        }
    }
}
var reissueid;

async function getStatus(status, empid) {    
    if (status === 'Pending') {        
        const employee = await $.ajax({
            url: HOST_URL + "/" + dappid + "/employeeData",
            type: 'post',
            data: '{"empid":"' + empid + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });        
        employeeid = empid
        employeeCertificate = employee;
        console.log(employee);
        document.getElementById('tabclick').click();
        fillDetails(employee);
        employeeid = undefined;       
    }
    else if(status=='Rejected')
    {
        reissueid=empid;
            $("#rejectpopup").modal('show');
    }
}


async function reissuepayslip()
{
    const employee = await $.ajax({
        url: HOST_URL + "/" + dappid + "/employeeData",
        type: 'post',
        data: '{"empid":"' + reissueid + '"}',
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
    });
    // alert(employee);
    employeeid = reissueid
    employeeCertificate = employee;
    console.log(employee);
    document.getElementById('tabclick').click();
    fillDetails(employee);
    employeeid = undefined;
}

function setPid(p, e) {
    pid = p;
    selectedEmpid = e;
}

async function fillDetails(res) {
    console.log(month + year);
    emp=res.employee
    document.getElementById("employeeid").value = emp.empid;
    document.getElementById("name").value = emp.name;
    document.getElementById("designation").value = emp.designation;
    document.getElementById("bank").value = emp.bank;
    document.getElementById("accountnumber").value = emp.accountNumber;
    var identityval;
    for(p in emp.identity) {
        console.log (p+emp.identity[p]);
        // $("#pan").attr("placeholder",p);
        identityval=emp.identity[p]
        $("#pan").attr("placeholder",p);     
    }
    document.getElementById("pan").value = identityval;
    document.getElementById("salary").value = emp.salary;
    document.getElementById("date").value = month + '-' + year;
}

function generate() {
    var email, empid, name, designation, bank, accountnumber, pan, salary, lta, ma, hra, pf, pt;

    empid = document.getElementById("employeeid").value;
    name = document.getElementById("name").value;
    designation = document.getElementById("designation").value;
    bank = document.getElementById("bank").value;
    accountnumber = document.getElementById("accountnumber").value;
    pan = document.getElementById("pan").value;
    salary = document.getElementById("salary").value;    
    document.getElementById("ps1_empname").textContent = name;
    document.getElementById("ps_pay").textContent = salary;
    document.getElementById("ps_designation").textContent = designation;
    document.getElementById("ps_idnum").textContent = empid;
    document.getElementById("ps_paycycle").textContent = month + '-' + year;
    document.getElementById("ps_bankdetails").textContent = bank;
    document.getElementById("basic").textContent = salary;    
    var earnings = [];
    var deductions = [];
    var otherearnings=[];
    var otherdeductions=[];
    var earnval=[];
    var dedval=[];
    var otherearnval=[];
    var otherdedval=[];
    var totalearnings=0;
    var totaldeductions=0;
    var totalotherearnings=0;
    var totalotherdeductions=0;
    var e = document.getElementById("dynamic_field");
    var n = e.rows.length;
    console.log(n);
    var e1 = document.getElementById("dynamic_field1");
    var n1 = e1.rows.length;
    console.log(n1);
    var e2 = document.getElementById("dynamic_field2");
    var n2 = e2.rows.length;
    console.log(n2);
    var e3 = document.getElementById("dynamic_field3");
    var n3 = e3.rows.length;
    console.log(n3);
    var table = document.getElementById('earnings');
    while (table.children[0].childElementCount > 3) {
        console.log('Deleting');
        table.children[0].children[2].remove();
    }
    for (i = 0; i < n - 1; i++) {
        var r=document.getElementById("dynamic_field");
        earnings[i] = r.children[0].children[i+1].cells[0].firstElementChild.value;
        earnval[i]=r.children[0].children[i+1].cells[1].firstElementChild.value;
        add_earnings1(earnings[i],earnval[i], i);
        totalearnings=totalearnings+Number(earnval[i]);
    }
    var table1 = document.getElementById('deductions');
    while (table1.children[0].childElementCount > 2) {
        console.log('Deleting');
        table1.children[0].children[1].remove();
    }
    for (j = 0; j < n1 - 1; j++) {
        var l=document.getElementById("dynamic_field1")
        deductions[j] =l.children[0].children[j+1].cells[0].firstElementChild.value;
        dedval[j]=l.children[0].children[j+1].cells[1].firstElementChild.value;
        add_deductions(deductions[j],dedval[j], j)
        totaldeductions=totaldeductions+Number(dedval[j]);
    }
    var table2 = document.getElementById('otherearnings');
    while (table2.children[0].childElementCount > 2) {
        console.log('Deleting');
        table2.children[0].children[1].remove();
    }
    for (p = 0; p < n2 - 1; p++) {
        var l=document.getElementById("dynamic_field2")
        otherearnings[p] =l.children[0].children[p+1].cells[0].firstElementChild.value;
        otherearnval[p]=l.children[0].children[p+1].cells[1].firstElementChild.value;
        add_otherEarnings(otherearnings[p],otherearnval[p], p)
        totalotherearnings=totalotherearnings+Number(otherearnval[p]);
    }
    var table3= document.getElementById('otherdeductions');
    while (table3.children[0].childElementCount > 2) {
        console.log('Deleting');
        table3.children[0].children[1].remove();
    }
    for (q= 0; q < n3 - 1; q++) {
        var l=document.getElementById("dynamic_field3")
        otherdeductions[q] =l.children[0].children[q+1].cells[0].firstElementChild.value;
        otherdedval[q]=l.children[0].children[q+1].cells[1].firstElementChild.value;
        add_otherDeductions(otherdeductions[q],otherdedval[q], q)
        totalotherdeductions=totalotherdeductions+Number(otherdedval[q]);
    }
 console.log(totalearnings)
 console.log(totaldeductions)
 document.getElementById("totalearnings").textContent=totalearnings+Number(salary);
 document.getElementById("totaldeductions").textContent=totaldeductions;
 document.getElementById("othertotalearnings").textContent=totalotherearnings;
 document.getElementById("othertotaldeductions").textContent=totalotherdeductions;
 var net=Number(salary)+(totalearnings+totalotherearnings)-(totaldeductions+totalotherdeductions);
 console.log(net)
 document.getElementById("netsalary").textContent = net;
 const value = inWords(net).toUpperCase();
  console.log(value);
 document.getElementById("words").textContent = value;
 earningsobject=createobject(earnings,earnval);
 console.log(earningsobject);
 deductionsobject=createobject(deductions,dedval);
 console.log(deductionsobject);
 otherearningsobject=createobject(otherearnings,otherearnval);
 console.log(otherearningsobject)
 otherdeductionsobject=createobject(otherdeductions,otherdedval);
 console.log(otherdeductionsobject)
}

function createobject(ear,val){
    var obj={}
  for(i=0;i<ear.length;i++){
     key=ear[i];
     obj[key]=String(val[i]);
  }
  return obj;
  }

function add_earnings1(earnings,earnvalue, id) {
    console.log(id);
    val = id + 2;
    console.log(val);
    console.log('Adding');
    var table = document.getElementById('earnings');
    var row = table.insertRow(id + 2);
    var cell1 = row.insertCell(0);
    var cell2=row.insertCell(1);
    cell1.innerHTML = earnings;
    cell2.innerHTML=earnvalue;

}

function add_deductions(deductions,dedvalue, id) {
    console.log(id);
    val = id + 1;
    console.log(val)
    var table = document.getElementById('deductions');
    var row = table.insertRow(id + 1);
    var cell1 = row.insertCell(0);
    var cell2=row.insertCell(1);
    cell1.innerHTML = deductions;
    cell2.innerHTML=dedvalue;
}

function add_otherEarnings(earnings,earnvalue, id) {
    console.log(id);
    val = id + 1;
    console.log(val)
    var table = document.getElementById('otherearnings');
    var row = table.insertRow(id + 1);
    var cell1 = row.insertCell(0);
    var cell2=row.insertCell(1);
    cell1.innerHTML = earnings;
    cell2.innerHTML=earnvalue;
}

function add_otherDeductions(deductions,dedvalue, id) {
    console.log(id);
    val = id + 1;
    console.log(val)
    var table = document.getElementById('otherdeductions');
    var row = table.insertRow(id + 1);
    var cell1 = row.insertCell(0);
    var cell2=row.insertCell(1);
    cell1.innerHTML = deductions;
    cell2.innerHTML=dedvalue;
}

function inWords(num) {
    var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

function hideshow() {

    var phrase = document.getElementById('passphrase');

    if (phrase.type === "password") {
        phrase.type = "text";
    }
    else { phrase.type = "password" }


}
// setInterval(function(){
// var date = document.getElementById('MonthYear').value;
// if(prev!= date){
//     getEmployees();
//     prev=date;
// }
// },2000);

async function initialIssue() {
 $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="font-size:70px; color: #00bfff"></i>',css: {backgroundColor: 'transparent', border: '0'} });
  var flag=await passphraselength();
  if(flag===true){
    empid = document.getElementById('employeeid').value;
    name = document.getElementById('name').value
    employer= company    
    month= month
    year = year
    designation= document.getElementById('designation').value
    bank = document.getElementById('bank').value
    accountNumber = document.getElementById('accountnumber').value
    grossSalary= document.getElementById('basic').innerText
    totalDeductions = document.getElementById('totaldeductions').innerText

    netSalary = document.getElementById('netsalary').innerText
    issuerid = iid
    secret = document.getElementById('passphrase').value
    template=document.getElementById('certtemplate').innerHTML;
    var issuedetails = {
        empid:String(empid),
        employer:String(employer),
        month:String(month),
        year:String(year),
        designation:String(designation),
        bank:String(bank),
        accountNumber:String(accountNumber),
        earnings:earningsobject,
        deductions:deductionsobject,
        otherEarnings:otherearningsobject,
        otherDeductions:otherdeductionsobject,
        grossSalary:String(grossSalary),
        totalDeductions:String(totalDeductions),
        netSalary:String(netSalary),
        issuerid:String(issuerid),
        secret:String(secret),
        template:template
    };
   
    console.log(document.getElementById('certtemplate').innerHTML);    
    console.log(JSON.stringify(issuedetails))
    const result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/payslip/initialIssue",
        type: 'post',
        data: JSON.stringify(issuedetails),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
    });
    console.log(result);
if(result.isSuccess==true){
    $('#errormsg').text(result.message);
    $('#ErrorModal').modal('show');
}
    else if (result.isSuccess === undefined) {        
        $('#errormsg').text("Connection Error");
        $('#ErrorModal').modal('show');
        document.getElementById('tagclose3').click();
        return;
    }
    else if (!result.isSuccess) {
    $('#errormsg').text(result.message);
        // $('#errormsg').text("EmployeeId : " + issuedetails['empid'] + ' is not issued');
        $('#ErrorModal').modal('show');
        return;
    }
    console.log("IT was a success");
    document.getElementById(issuedetails["empid"] + 'status').innerText = "Initiated";
    document.getElementById(issuedetails["empid"] + 'status').setAttribute('data-target', '#popup');
    document.getElementById(issuedetails["empid"] + 'status').setAttribute('data-toggle', 'modal');
    document.getElementById(issuedetails["empid"] + 'status').setAttribute('onclick', '');
    document.getElementById(issuedetails["empid"] + 'status').setAttribute('class', calssButtons['Initiated']);
    document.getElementById('tagclose3').click();
    $.unblockUI();
}
}

//Register new employee functions

function getCountries() {
    $.get("http://54.254.174.74:8080/api/v1/countries", function (data) {
        // console.log(data);
        countryData = data;
        console.log(data);
        var x = document.getElementById("countrycode");
        for (var i = 0; i < count_countries; i++) {
            var option = document.createElement("option");
            option.text = countryData['data'][i]['countryName'];
            x.add(option);
        }
    });
}

function countryValidator() {
    var e = document.getElementById("countrycode");
     console.log(e);
    var strUser = e.options[e.selectedIndex].text;
    // console.log(e.options[e.selectedIndex]);
    var i = e.selectedIndex - 1;
    console.log(i);
    console.log(strUser);
    console.log(countryData);
    console.log(countryData['data'][i]['countryCode']);
    countryCode1 = countryData['data'][i]['countryCode'];
    // console.log(countryCode)
    console.log(countryData['data'][i]['countryID']);
}
  async function getdepList(){
    console.log("in dep list")
    let result;
    try {
       result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/department/get",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(JSON.stringify(result)+"sdrtfgyuhji");
        return result;
       
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
  }

async function getdesList()
{
    let result;
    try {
       result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/employees/getDesignations",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        // console.log(JSON.stringify(result)+"sdrtfgyuhji");
        return result;
       
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}


async function getBanksList()
{
    let banks;
    try {
       banks = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "getBanks",
            type: 'post',
            data: JSON.stringify(registerData),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        // banks={
        //     "banks": [
        //         "State Bank of India",
        //         "HDFC",
        //         "AXIS",
        //         "IDBI",
        //         "Other"
        //     ],
        //     "isSuccess": true,
        //     "success": true
        // };
        var banksDropdown = document.getElementById("rbank");
        if(banks.isSuccess===true)
        {
            for(i in banks.banks) {
                banksDropdown.options[banksDropdown.options.length] = new Option(banks.banks[i], i);
                // console.log(i+"dfghj"+banksDropdown.options.length)
            }
            // console.log(banksDropdown.options.length)
            banksDropdown.options[banksDropdown.options.length] = new Option("Other", banksDropdown.options.length-2);

        }
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}
var id;
function bankValidator()
{
    console.log("in bank validator");
    var banksDropdown = document.getElementById("rbank");
    var selectedBank=banksDropdown.options[banksDropdown.selectedIndex].text;
    if(selectedBank==="Other")
    {        
        // document.getElementById("rbank").style.display = "none";
        document.getElementById("otherbank").style.display = "block";
        id="otherbank";
        // bankName=document.getElementById("otherbank").value;
    }
    else
    {
        document.getElementById("otherbank").style.display = "none";
        // bankName=banksDropdown.options[banksDropdown.selectedIndex].text;
        id="rbank"
    }
}
var desid
function desvalidator(){
    var e = document.getElementById("designation1");
    var des=e.options[e.selectedIndex].text;
    if(des==="Other")
    {
        desid="otherdes"
        document.getElementById("otherdes").style.display = "block";
    }
    else
    {
        desid="designation1"
        document.getElementById("otherdes").style.display = "none";
    }
}

async function register() {
    var flag=await empregformValidation();
    if(flag===true){                
    $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="z-index: 99999 !important; font-size:70px; color: #00bfff"></i>',css: {backgroundColor: 'transparent', border: '0'} });
    const res3 = await registerEmployee();
    console.log(res3);
    if (res3.isSuccess === true) {
        // $("#reg-new-emp").modal('hide');
        if(res3['message']=='Awaiting wallet address'){
document.getElementById('press2').click();
employeeid = document.getElementById("empid1").value;

        }
        else{
            flag = true;
            employeeid = document.getElementById("empid1").value;
            console.log(employeeid);
            document.getElementById("registered").click();
             getEmployees(month, year, ge_limit,ge_offset,selecteddesfilter);
        }
       
    }
    else {
        $("#reg-new-emp").modal('show');
        employeeid = undefined;
        var msg = JSON.parse(res3.message)     
        document.getElementById("msg").textContent = msg.message;
    }
    $.unblockUI();
}
}


async function registerEmployee() {
    var bankName,designation;
    console.log(identityproof)
    var idvalue=document.getElementById("pannum").value
    var identity1=createidentityobject(identityproof,idvalue)
    var e = document.getElementById("department");
    var department=e.options[e.selectedIndex].text;
console.log(id);
if(id==="rbank"){
    e=document.getElementById("rbank");
    bankName=e.options[e.selectedIndex].text;
}
else if(id==="otherbank"){    
    bankName=document.getElementById("otherbank").value;
}
if(desid==="designation1"){
    var e = document.getElementById("designation1");
 designation=e.options[e.selectedIndex].text;
}
else if(desid==="otherdes"){
    designation=document.getElementById("otherdes").value;    
}
    console.log(countryCode1);
    registerData = {
        countryCode: countryCode1,
        empid: document.getElementById("empid1").value,
        email: document.getElementById("email").value,
        lastName: document.getElementById("lname").value,
        name: document.getElementById("fname").value,
        designation:designation,
        bank: bankName,
        accountNumber: document.getElementById("accnum").value,
        identity:identity1,
        salary: document.getElementById("salary1").value,
        dappid: dappid,
        token: "",
        groupName: "Dapps",
        iid:iid,
        department:department
    };
    console.log(registerData);
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "registerEmployee",
            type: 'post',
            data: JSON.stringify(registerData),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

function createidentityobject(identity,num){
    var obj={}
  
     key=identity;
     obj[key]=num;
  return obj;
  }

async function finalIssue() {
    var flag=await finalpassphrase();
    if(flag===true){    
    var payslip = {
        "pid": pid,
        "fee": "0",
        "iid": iid,
        "secret": document.getElementById('passphrase1').value,
        "dappid": dappid
    };
    let result;
    console.log(payslip);
    $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="font-size:70px; color: #00bfff"></i>', css: { backgroundColor: 'transparent', border: '0' } });
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/issueTransactionCall",
            type: 'post',
            data: JSON.stringify(payslip),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(result);
        if (result.isSuccess == false) {            
            $('#errormsg').text("Failed to issue");
            $('#ErrorModal').modal('show');
            return;
        }
        else{
            $('#errormsg').text("Successfully Issued Payslip");
            $('#ErrorModal').modal('show');
        }
        if (result.transactionId != undefined) {            
            document.getElementById(selectedEmpid + "status").value = 'Issued';
            document.getElementById(selectedEmpid + 'status').setAttribute('id', '#popup2');
            // document.getElementById('tagclose4').click();
        }

    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
    $.unblockUI();
}
}

function escapeInput(input) {
    return String(input)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}



async function getcustomfields(){
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/customFields/get",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        if(result.isSuccess===true){
            var i=document.getElementById("dynamic_field").rows.length;
            j=i-1
            var key1=result.earnings
            for(p in key1) {
                target='';
                name='';
                if(key1[p].flag== 'compulsory'){
                  target= ' data-toggle="modal" data-target="#fieldpopup" ';
                  name='compulsory';
                }
                else{
                    name='optional';
                }

                i++;
                $('#dynamic_field').append('<tr id="row'+i+'"><td><input id="input'+j+'" type="text" name="name[]" placeholder="Enter field name" class="form-control name_list" /></td><td><input id="field'+j+'" type="text" name="name[]" placeholder="Enter value" class="form-control name_list" /></td><td><button type="button" name="'+ name +'" id="'+i+'" '+ target +' class="btn btn-danger btn_remove">X</button></td></tr>');
                l = document.getElementById("dynamic_field")
                l.children[0].children[i-1].cells[0].firstElementChild.value=key1[p].name;
                j++;   
            }
            var x=document.getElementById("dynamic_field1").rows.length;
            y=x-1;
            var key2=result.deductions;
            for(p in key2) {
                target='';
                name='';
                if(key2[p].flag== 'compulsory'){
                    target= ' data-toggle="modal" data-target="#fieldpopup" ';
                    name='compulsory';
                  }
                  else{
                    target='';
                      name='optional';
                  }
                x++;
                $('#dynamic_field1').append('<tr id="rows'+x+'"><td><input id="inputs'+y+'" type="text" name="name[]" placeholder="Enter Field name" class="form-control name_list" /></td><td><input id="fields'+y+'" type="text" name="name[]" placeholder="Enter value" class="form-control name_list" /></td><td><button type="button" name="'+ name +'" id="'+x+'" '+ target +'  class="btn btn-danger btn_remove1">X</button></td></tr>');
                l = document.getElementById("dynamic_field1")
                l.children[0].children[x-1].cells[0].firstElementChild.value=key2[p].name;
                y++;
            }
            var r=document.getElementById("dynamic_field2").rows.length;
        s=r-1;
            var key3=result.otherEarnings;
            for(p in key3) {
                target='';
                name='';
                if(key3[p].flag== 'compulsory'){
                    target= ' data-toggle="modal" data-target="#fieldpopup" ';
                    name='compulsory';
                  }
                  else{
                      name='optional';
                  }
                r++;
                $('#dynamic_field2').append('<tr id="roww'+r+'"><td><input id="inputt'+s+'" type="text" name="name[]" placeholder="Enter Field name" class="form-control name_list" /></td><td><input id="fieldd'+s+'" type="text" name="name[]" placeholder="Enter value" class="form-control name_list" /></td><td><button type="button" name="'+ name +'" id="'+r+'" '+ target +' class="btn btn-danger btn_remove2">X</button></td></tr>');
                l = document.getElementById("dynamic_field2")
                l.children[0].children[r-1].cells[0].firstElementChild.value=key3[p].name;
                s++;
            }
            var a = document.getElementById("dynamic_field3").rows.length;
            b = a - 1;
            var key4 = result.otherDeductions;
            for (p in key4) {
                a++;
                target='';
                name='';
                if(key4[p].flag== 'compulsory'){
                    target= ' data-toggle="modal" data-target="#fieldpopup" ';
                    name='compulsory';
                  }
                  else{
                      name='optional';
                  }
                $('#dynamic_field3').append('<tr id="rowws' +a+'"><td><input id="inputss'+b+'" type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input id="fieldd'+b+'" type="text" name="name[]" placeholder="Enter value" class="form-control name_list" /></td><td><button type="button" name="'+ name +'" id="'+a+'" '+ target +' class="btn btn-danger btn_remove3">X</button></td></tr>');
                l = document.getElementById("dynamic_field3")
                l.children[0].children[a-1].cells[0].firstElementChild.value=key4[p].name;
                b++;
            }
            key5=result.identity
            for(p in key5) {
                 identityproof=p;
                $("#pannum").attr("placeholder",p);     
            }

        }
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }  

}


async function getdes(){
    let result;
    try {
       result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/employees/getDesignations",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}




