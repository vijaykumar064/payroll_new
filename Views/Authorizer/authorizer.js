//var secret="cactus peasant return inside filter morning wasp floor museum nature iron can";
var pid1;
var clickedpid = undefined;
var dappid = localStorage.getItem("dappid");
var aid = localStorage.getItem("authid");
var role = localStorage.getItem("roleId");
var company = localStorage.getItem("companyname");
// var category = localStorage.getItem("category");
var email = localStorage.getItem("email");
var belriumtoken = localStorage.getItem("belToken");
var dappid = localStorage.getItem("dappid");
var bel = localStorage.getItem("bel");
var aps_limit=5,aps_offset=0,ap_limit=5,ap_offset=0,rp_limit=5,rp_offset=0;

async function authorizer() {
  var query=decodeURIComponent(window.location.search);
  query=query.substring(1);
  if(query==="pendingsigns")
  {
    document.getElementById('authorizeTab').click();
  }
  else if(query==="payslipsrejected")
  {
    document.getElementById('rejectedTab').click();
  }
  else if(query==="payslipsapproved")
  {
    document.getElementById('authorizedTab').click();
  }
  authorizePayslips(aid,aps_limit,aps_offset);
}

async function authorizePayslips(aid,aps_limit,aps_offset)
{
  var params = {
    aid: aid,
    limit: aps_limit,
    offset: aps_offset
  };
  var data=[];
  const result = await pendingSigns(params);
  console.log(JSON.stringify(result));
  if (result.isSuccess === true) {
    var n = result.result;
    len = n.length;
    var i;
    for (i = 0; i < len; i++) {
      var myDate = new Date(Number(n[i].timestampp));
      var time = myDate.toGMTString();
      data.push( [`<button type="button" class="btn btn-sendmoney" onclick="displayPayslipdata(` + n[i].pid + `)">`+n[i].pid+`</button>`, n[i].email, n[i].iid, time] );
    }
  }
  console.log(data);
  $('#authorizeTable').DataTable( {
      data:           data,
      pagingType: "simple",
      deferRender:    true,
      //scrollY:        200,
      //scrollCollapse: true,
      //scroller:       true,
      destroy: true,
      drawCallback: function(){
          if(aps_offset!=0)
          {
              $('#authorizeTable_previous').removeClass('disabled');
          }
          if((aps_limit+aps_offset)<result.total)
          {
              $('#authorizeTable_next').removeClass('disabled');
          }
          
          $('.paginate_button.next:not(.disabled)', this.api().table().container())          
             .on('click', async function(){
              //alert('next');
              aps_offset+=aps_limit;
              await authorizePayslips(aid,aps_limit,aps_offset);
              
             });
             $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
             .on('click', async function(){
                //alert('previous');
                aps_offset-=aps_limit;
                await authorizePayslips(aid,aps_limit,aps_offset);
             });        
       }
    });
  }

function takedata(pid) {
  clickedpid = pid;
}

async function pendingSigns(param) {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "authorizers/pendingSigns",
      type: 'post',
      data: JSON.stringify(param),
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
  }
}
async function sign() {
  $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="font-size:70px; color: #00bfff"></i>',css: {backgroundColor: 'transparent', border: '0'} });
  const signRes = await authorize(clickedpid);
  console.log(JSON.stringify(signRes));
  if (signRes.isSuccess === true) {
    setTimeout(authorizer(), 2000);
  }
  $.unblockUI();
    $('#errormsg').text(signRes.message);
    $('#ErrorModal').modal('show');
}

async function authorize(pid) {
  const secret = document.getElementById("passphrase1").value;
  console.log(secret);
  console.log(pid);
  var params = {
    aid: aid,
    pid: String(pid),
    secret: secret
  }
  console.log(JSON.stringify(params));
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "authorizer/authorize",
      type: 'post',
      data: JSON.stringify(params),
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
  }
}

async function reject() {
  const rejectRes = await rejectpayslip(clickedpid);
  if(rejectRes.success===true)
  {
    $('#errormsg').text("Rejected Successfully");
    $('#ErrorModal').modal('show'); 
  }
  else
  {
    $('#errormsg').text("Failed to Reject");
    $('#ErrorModal').modal('show'); 
  }
}

async function rejectpayslip(pid) {
  var data = {
    pid: pid.toString(),
    aid: aid,
    message: $('#reason').val()
  }
  console.log(JSON.stringify(data));
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "authorizer/reject",
      type: 'post',
      data: JSON.stringify(data),
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    console.log(result);
    return result;

  }
  catch (error) {
    if (error) {
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
  }
}

async function authorizedAssets(ap_limit,ap_offset) {
  const res1 = await authorizedPayslips();
  var n,i,data=[];
  console.log(JSON.stringify(res1));
  if (res1.isSuccess === true) {
    n = res1.result;
    len = n.length;
    for (i = 0; i < len; i++) {
      var myDate = new Date(Number(n[i].timestampp));
      var time = myDate.toGMTString();
      data.push( [n[i].pid,n[i].email, n[i].iid, time] );
    }
  console.log(data);
  $('#issuedTable').DataTable( {
      data:           data,
      pagingType: "simple",
      deferRender:    true,
      //scrollY:        200,
      //scrollCollapse: true,
      //scroller:       true,
      destroy: true,
      drawCallback: function(){
          if(ap_offset!=0)
          {
              $('#issuedTable_previous').removeClass('disabled');
          }
          if((ap_limit+ap_offset)<n.total)
          {
              $('#issuedTable_next').removeClass('disabled');
          }
          
          $('.paginate_button.next:not(.disabled)', this.api().table().container())          
             .on('click', async function(){
              //alert('next');
              ap_offset+=ap_limit;
              await authorizePayslips(ap_limit,ap_offset);
              
             });
             $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
             .on('click', async function(){
                //alert('previous');
                ap_offset-=ap_limit;
                await authorizePayslips(ap_limit,ap_offset);
             });        
       }
    });
  }
  }

function hideshow() {

  var phrase = document.getElementById('passphrase1');

  if (phrase.type === "password") {
    phrase.type = "text";
  }
  else { phrase.type = "password" }


}

// function add_authorizedemployee(pid, emp, iid, time) {
//   console.log(pid);
//   add_div = '<tr><td>' + pid + '</td> <td>' + emp + '</td><td>' + iid + '</td> <td>' + time + '</td> </tr>'
//   console.log(add_div);
//   document.getElementById('payslips').innerHTML += add_div;
// }
async function authorizedPayslips() {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "authorizer/authorizedAssets",
      type: 'post',
      data: '{"aid":"' + aid + '"}',
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
  }
}

async function displayPayslipdata(pid) {
  clickedpid=pid;
  pid1 = pid;
  const payslipData = await getPayslipData(pid);
  console.log(payslipData);
  console.log(payslipData.result.name);
  paycycle = payslipData.result.month + ", " + payslipData.result.year;
  
  try{
  document.getElementById("ps_empname").value = payslipData.result.name;
  document.getElementById("ps_pay").value = payslipData.result.accountNumber;
  document.getElementById("ps_designation").value = payslipData.result.designation;
  document.getElementById("ps_idnum").value = payslipData.result.empid;
  document.getElementById("ps_paycycle").value = paycycle;
  document.getElementById("ps_bankdetails").value = payslipData.result.bank;
  document.getElementById("netsalary").textContent = payslipData.result.netSalary;
  const value = inWords(payslipData.result.netSalary).toUpperCase();
  console.log(value);
  document.getElementById("words").textContent = value;
  var id_table = document.getElementById("idTable");
  var earnings_table = document.getElementById("earningsTable");
  var deductions_table = document.getElementById("deductionsTable");
  var otherearnings_table=document.getElementById("otherearningsTable");
  var otherdeductions_table=document.getElementById("otherdeductionsTable");
  $("#idTable").find("tr:not(:first)").remove();
  $("#earningsTable").find("tr:not(:first)").remove();
  $("#deductionsTable").find("tr:not(:first)").remove();
  $("#otherearningsTable").find("tr:not(:first)").remove();
  $("#otherdeductionsTable").find("tr:not(:first)").remove();
  
  for(var i in payslipData.result.identity){
    var idRow=document.createElement("tr");
    var idCol1=document.createElement("td");
    var name=document.createTextNode(i);
    idCol1.appendChild(name);
    var idCol2=document.createElement("td");
    var idval=document.createTextNode(payslipData.result.identity[i]);
    idCol2.appendChild(idval);
    idRow.appendChild(idCol1);
    idRow.appendChild(idCol2);
    id_table.appendChild(idRow);
  }
  for(var i in payslipData.result.earnings){
    var earnRow=document.createElement("tr");
    var earnCol1=document.createElement("td");
    var name=document.createTextNode(i);
    earnCol1.appendChild(name);
    var earnCol2=document.createElement("td");
    var earnVal=document.createTextNode(payslipData.result.earnings[i]);
    earnCol2.appendChild(earnVal);
    earnRow.appendChild(earnCol1);
    earnRow.appendChild(earnCol2);
    earnings_table.appendChild(earnRow);
  }
  for(var i in payslipData.result.deductions){
    var dedRow=document.createElement("tr");
    var dedCol1=document.createElement("td");
    var name=document.createTextNode(i);
    dedCol1.appendChild(name);
    var dedCol2=document.createElement("td");
    var dedVal=document.createTextNode(payslipData.result.deductions[i]);
    dedCol2.appendChild(dedVal);
    dedRow.appendChild(dedCol1);
    dedRow.appendChild(dedCol2);
    deductions_table.appendChild(dedRow);
  }
  for(var i in payslipData.result.otherEarnings){
    var dedRow=document.createElement("tr");
    var dedCol1=document.createElement("td");
    var name=document.createTextNode(i);
    dedCol1.appendChild(name);
    var dedCol2=document.createElement("td");
    var dedVal=document.createTextNode(payslipData.result.otherEarnings[i]);
    dedCol2.appendChild(dedVal);
    dedRow.appendChild(dedCol1);
    dedRow.appendChild(dedCol2);
    otherearnings_table.appendChild(dedRow);
  }
  for(var i in payslipData.result.otherDeductions){
    var dedRow=document.createElement("tr");
    var dedCol1=document.createElement("td");
    var name=document.createTextNode(i);
    dedCol1.appendChild(name);
    var dedCol2=document.createElement("td");
    var dedVal=document.createTextNode(payslipData.result.otherDeductions[i]);
    dedCol2.appendChild(dedVal);
    dedRow.appendChild(dedCol1);
    dedRow.appendChild(dedCol2);
    otherdeductions_table.appendChild(dedRow);
  }
  }
  catch(error){
    console.log(error);
    $('#errormsg').text(error.responseText);
    $('#ErrorModal').modal('show');
  }
  $('#ShowPayslipPop').modal('show');
}

function inWords(num) {
  var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
  var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
  if ((num = num.toString()).length > 9) return 'overflow';
  n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  if (!n) return; var str = '';
  str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
  str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
  str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
  str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
  str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
  return str;
}

async function getPayslipData(pid) {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "payslip/getPayslip",
      type: 'post',
      data: '{"pid":"' + pid + '"}',
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
      $('#errormsg').text(error.responseText);
      $('#ErrorModal').modal('show');
    }
  }
}

function signhere() {
  console.log(pid1);
  sign(pid1);
}

function authorizerDashboard() {

  document.getElementById("username").innerText = email;
  document.getElementById("role").innerHTML = role.toUpperCase();
  document.getElementById("balance").innerText = (bel + " " + "BEL");
  document.getElementById("company").innerText = company;
  // document.getElementById("category").innerText=category;
}

//rejectedpayslips function
async function rejectedPayslips(rp_limit,rp_offset)
{
  const Res_rejectPayslips=await getrejectedPayslips();
  var data=[];
  if(Res_rejectPayslips.isSuccess===true)  
  {
    Res_rejectPayslips.rejectedDetails.forEach(e => {
      data.push( [e.pid,e.iid,e.issuedBy,e.employee,e.month,e.year,e.reason] );
      //console.log(JSON.stringify(e));
    });
  console.log(data);
$('#rejectedTable').DataTable( {
    data:             data,
    pagingType:       "simple",
    deferRender:      true,
    //scrollY:        200,
    //scrollCollapse: true,
    //scroller:       true,
    destroy: true,
    drawCallback: function(){
        if(rp_offset!=0)
        {
            $('#rejectedTable_previous').removeClass('disabled');
        }
        if((rp_limit+rp_offset)<Res_rejectPayslips.total)
        {
            $('#rejectedTable_next').removeClass('disabled');
        }
        
        $('.paginate_button.next:not(.disabled)', this.api().table().container())          
           .on('click', async function(){
            //alert('next');
            rp_offset+=rp_limit;
            await rejectedPayslips(rp_limit,rp_offset);
            
           });
           $('.paginate_button.previous:not(.disabled)', this.api().table().container())          
           .on('click', async function(){
              //alert('previous');
              rp_offset-=rp_limit;
              await rejectedPayslips(rp_limit,rp_offset);
           });        
     }
  });
}
}//end of rejectedpayslips function

//getrejectedpayslips function
async function getrejectedPayslips()
{
    let result;
    try {
      result = await $.ajax({
        url: HOST_URL + "/" + dappid + "/" + "authorizer/rejecteds",
        type: 'post',
        data: '{"aid":"' + aid + '"}',
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json'
      });
      return result;
    }
    catch (error) {
      if (error) {
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
      }
    }
}//end of getrejectedpayslips function