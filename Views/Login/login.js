var token, secret, email, password, dappid;
function model() {
    var email = localStorage.getItem("email");
    console.log(email);
    if (email != null) {
        a = document.getElementById("emailid");
        a.value = email;
        a.disabled = "true";
    }
}

async function login1() {
    var flag = await loginValidation();
    console.log(flag);
    if (flag === true) {
        console.log("inside login")
        const loginResponse = await checkLogin();
        console.log(loginResponse);

        if (loginResponse["isSuccess"] === true) {
            console.log(loginResponse);
            console.log(loginResponse.data.token);
            token = loginResponse.data.token;
            localStorage.setItem("belToken", token);
            hyperLedgerLogin(token);
        }
        else {
            // alert(loginResponse);
            $("#message").text(loginResponse["message"]);
        }
    }
}

async function checkLogin() {
    email = escapeInput($("#emailid").val());
    localStorage.setItem('email',email);
    password = escapeInput($("#Password").val());
    console.log(email + password);
    var totp = "";
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + SDAPP_ID + "/" + "user/login",
            data: '{"email":"' + email + '","password":"' + password + '","totp":"' + totp + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            //console.log(error);
            if ((error["status"] === 401) && (error["responseJSON"]["status"] === "UNAUTHORIZED")) {
                //   alert("error" + error["responseJSON"]["message"]);
                $('#errormsg').text(error["responseJSON"]["message"]);
                $('#ErrorModal').modal('show');
            } else {
                //   alert("error" + error["responseJSON"]["message"]);
                $('#errormsg').text(error["responseJSON"]["message"]);
                $('#ErrorModal').modal('show');
            }
        }
    }
}  //end of check login function

//hyperLedgerLogin function
async function hyperLedgerLogin(token) {
    $("#popup").modal("show");
    $("#secret").click(async function () {
        var secret = escapeInput($("#secretkey").val());
        console.log("secret is : " + secret);
        localStorage.setItem("secret", secret);
        $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="font-size:70px; color: #00bfff"></i>', css: { backgroundColor: 'transparent', border: '0' } });
        const hllogin = await checkHyperLogin(secret, token);
        console.log(hllogin);
        var k = JSON.parse(hllogin.data);
        // if(true){
        if (hllogin["isSuccess"] === true) {

            $("#popup").modal("hide");
            localStorage.setItem("bkvsdm_token", k.token);
            getKycStatus();
            const res4 = await getAddress();
            console.log("res4: " + JSON.stringify(res4));
            var address = res4.data.countries[0].wallets[0].address;
            console.log(address);
            localStorage.setItem("address", address);
            const res5 = await getBalance(address);
            if(res5.isSuccess===true){
            console.log("res5 :" + JSON.stringify(res5));
            var balance = JSON.parse(res5.data).balance;
            bel = balance / 10000000000;
            console.log(bel);
            localStorage.setItem("bel", bel);
            }
            else{
                localStorage.setItem("bel","0");
            }
            getRole();
        }
        else {
            console.log(hllogin.message);
            $("#errmsg").text(hllogin.message);
        }
        $.unblockUI();
    });
} //end of hyperLedgerLogin function

//checkHyperLogin function
async function checkHyperLogin(secret, token) {
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + SDAPP_ID + "/" + "user/hllogin",
            data: '{"secret":"' + secret + '","token":"' + token + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            $('#errormsg').text(error.responseText);
            $('#ErrorModal').modal('show');
        }
    }
}// end of checkHyperLogin function

async function getRole() {
    const data = await checkRole();
    console.log(JSON.stringify(data));
    // alert(JSON.stringify(data));
    if(!data.role){
        var roleid = data[0].role;
        dappid = data[0].dappid;
        // alert(dappid);
        // alert(JSON.stringify(data));
        localStorage.setItem("country", data[0].country);
        localStorage.setItem("companyname", data[0].company);
        localStorage.setItem("name", data[0].name);
        localStorage.setItem("roleId", roleid);
        localStorage.setItem("dappid", dappid);
        var kycStatus = localStorage.getItem("kycStatus");
        console.log("roleid :" + roleid + "kyc Status: " + kycStatus);
         if (roleid === "superuser") {
            const depresponse= await getdepartments();
            console.log(depresponse);
            if (kycStatus === "false") {
                window.location.href = "../Wallet/wallet.html";
            }
            else if(depresponse.success === true){
            depcount=depresponse.departments.length;
            if(depcount>1){
                const fieldsresponse=await getfields();
                console.log(JSON.stringify(fieldsresponse));
                if(fieldsresponse.success===true){
                    if(fieldsresponse.earnings!=""){
                        if(fieldsresponse.deductions!=""){
                                window.location.href = "../SuperAdmin/superadmin-issuers.html";
    
                        }
                    }
            }
            }
            else{
             window.location.href = "../Settings/settings.html";
            }
        }
        }
        else if (roleid === "issuer") {
            const res = await getissuerid();
            console.log(res.result.iid);
            localStorage.setItem("issuerid", res.result.iid);
            localStorage.setItem("issuerdepartments",res.result.departments);
            if (kycStatus === "false") {
                window.location.href = "../Wallet/wallet.html";
            }
            else{
                window.location.href = "../Dashboard/dashboard.html";

            }
        }
        else if (roleid === "authorizer") {
            const res = await getauthid();
            console.log(res);
            localStorage.setItem("authid", res.result.aid);
            if (kycStatus === "false") {
                window.location.href = "../Wallet/wallet.html";
            }
            else{
                window.location.href = "../Authorizer/authorizer.html";

            }
        }
    
    }
    else if (data.role === "new user") {
        localStorage.setItem("roleId", data.role);

            window.location.href = "../Wallet/wallet.html";
    }
     else{
         console.log("role undefined");
     }
}

async function checkRole() {
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + SDAPP_ID + "/" + "user/dappid",
            data: '{"email":"' + email + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        console.log(error);
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
    }
}

async function getauthid() {
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + dappid + "/" + "authorizers/getId",
            data: '{"email":"' + email + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        console.log(error);
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
    }
}

async function getissuerid() {
    let result;
    try {
        result = await $.ajax({
            type: 'post',
            url: HOST_URL + "/" + dappid + "/" + "issuers/getId",
            data: '{"email":"' + email + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        console.log(error);
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
    }
}

async function issuerdata(iid) {
    var params = {
        iid: iid
    }
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/issuer/data",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}


async function authdata(aid) {
    var params = {
        aid: aid
    }
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/authorizer/data",
            type: 'post',
            data: JSON.stringify(params),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}

async function CheckKYCStatus(token) {
    let result;
    try {
        result = await $.ajax({
            type: 'get',
            url: 'http://54.254.174.74:8080/api/v1/user/countries/kyc',
            headers: { "belrium-token": token }
        });
        return result;
    }
    catch (error) {
        console.log(error);
        $('#errormsg').text(error.responseText);
        $('#ErrorModal').modal('show');
    }
}

//getKycStatus function
async function getKycStatus() {
    if (email === 'vino1@yopmail.com') {
        kycStatus = "true";
        localStorage.setItem("kycStatus", kycStatus);
    }
    else if (email === 'vino2@yopmail.com') {
        kycStatus = "true";
        localStorage.setItem("kycStatus", kycStatus);
    }
    else if (email === 'vino3@yopmail.com') {
        kycStatus = "true";
        localStorage.setItem("kycStatus", kycStatus);
    }
    
    // else if (email === 'auth2@yopmail.com') {
    //     kycStatus = "true";
    //     localStorage.setItem("kycStatus", kycStatus);
    // }
    // else if (email === 'auth3@yopmail.com') {
    //     kycStatus = "true";
    //     localStorage.setItem("kycStatus", kycStatus);
    // }
    // else if (email === 'issuer2@yopmail.com') {
    //     kycStatus = "true";
    //     localStorage.setItem("kycStatus", kycStatus);
    // }
    // else if (email === 'issuer3@yopmail.com') {
    //     kycStatus = "true";
    //     localStorage.setItem("kycStatus", kycStatus);
    // }
    
    else {
        const kycstatus = await CheckKYCStatus(token);
        var obj = kycstatus.data[0].kycstatus;
        console.log("kycStatus: " + obj);
        localStorage.setItem("kycStatus", obj);
        // localStorage.setItem("kycStatus", "true"); 
    }
} //end of getKycStatus function

async function getAddress() {
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + SDAPP_ID + "/" + "user/wallet",
            type: 'post',
            data: '{"token":"' + token + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            $('#errormsg').text(error.responseText);
            $('#ErrorModal').modal('show');
        }
    }
}

async function getBalance(address) {
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + SDAPP_ID + "/" + "user/balance",
            type: 'post',
            data: '{"address":"' + address + '","belriumtoken":"' + token + '"}',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
            $('#errormsg').text(error.responseText);
            $('#ErrorModal').modal('show');
        }
    }
}

function escapeInput(input) {
    return String(input)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}


// { "issuer":
//  { "iid": "7", "publickey": "2eca2ded1136b56980f2c77c6ab8dfe8d89ddd97b0c4e7a2260eab160a685e5c", "email": "vinodno@yopmail.com", "designation": "Issuer", "timestampp": "1548070607583", "category": "tech", "deleted": "0" },
//   "employeesRegistered": 5,
//    "issuesCount": 6,
//     "isSuccess": true, 
//     "success": true }

async function getdepartments(){
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/department/get",
            type: 'post',
            data:{},
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }

}


async function getfields(){
    let result;
    try {
        result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/customFields/get",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        return result;
    }
        catch (error) {
            if (error) {
                console.log(error);
            }
        }  
}