//Home or Index page valiadations
function formValidation(){
  if(validateEmail()){
      return true;
  }
  return false;
  }
  function emailValidation(inputtext, alertMsg,id){
      var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
      if(inputtext.value.match(emailExp)){
          document.getElementById(id).innerText =" ";
          return true;
      }else{
          document.getElementById(id).innerText = alertMsg; //this segment displays the validation rule for email
          inputtext.focus();
          return false;
      }
  }
  function validateEmail(){
      var email =  document.getElementById('email');
      if(email.value!=""){
      var res=emailValidation(email, "Please enter a valid email address","p1")
      if(res){
          return true;
      }
      }
      else{
          document.getElementById('p1').innerText ="Email cannot be empty";
          return false;
      }
  }

// Admin dashboard validations :

// Issuers Registration:

// EmpId validation :
function validateEmpId(){
    var empId = document.getElementById('id');
                if(empId.value!=""){
                var res=valEmp(empId, "EmpId format incorrect","p1");
                if(res)
                return true;                
                }
                else{
                  document.getElementById('p1').innerText = "field mandatory";
                  return false;
                }
  }
  
  function valEmp(empid, alertMsg,id){
    var idVal = /^[0-9]+$/;
    if(empid.value.match(idVal))
          {   
            document.getElementById(id).innerText = "";
        return true;
          }
        else
          {
            document.getElementById(id).innerText = alertMsg;
            sal.focus();
            return false;
          }
  }
  
  // Email validation :
  function validateEmpEmail(){
    var email =  document.getElementById('email');
    if(email.value!=""){
    var res=emailValidation(email, "Please enter a valid email address","p2");
    if(res)
      return true;
    }
    else{
      document.getElementById('p2').innerText = "field mandatory";
      return false;
    }
  }
  
  // Designation validation :
  function validateDesignation(){
    var designation = document.getElementById('designation');
      if(designation.value!=""){
        var res=valDesignation(designation, "For your designation please use alphabets only","p3");
      if(res)
      return true;
      }
      else{
        document.getElementById('p3').innerText = "field mandatory";
      }
  }
  
  function valDesignation(text, alertMsg, id){ 
        var letters = /^[A-Za-z]+$/;
        if(text.value.match(letters))
        {
          document.getElementById(id).innerText = "";
        return true;
        }
        else
        {
          document.getElementById(id).innerText = alertMsg;
          text.focus();
        return false;
        }
  }
  
  // Name validation :
  function validateName(){
    var name = document.getElementById('name');
      if(name.value!=""){
        var res=valstring(name, "For your name please use alphabets only", "p4");
      if(res)
      return true;
      }
      else{
        document.getElementById('p4').innerText = "field mandatory";
      }
  }
  

  
  // Password validation :
  function validatePassword(){
    var password = document.getElementById('password');
      if(password.value!=""){
        var res=passwordValidate(password, "Please enter a password which contains atleast one Symbol,Digit,Uppercase and of range 7 to 14", "p5");
      if(res)
      return true;
      }
      else{
        document.getElementById('p5').innerText = "field mandatory";
      }
  }
  
  function passwordValidate(inputtext, alertMsg,id){
    var validpassword=/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,14}$/;
  if(inputtext.value.match(validpassword)){
      document.getElementById(id).innerText = "";
      return true;  
  }
  else{
      document.getElementById(id).innerText = alertMsg;
      inputtext.focus();
      return false;
  }
  }
  
  // Issuer registration form validation for Non-blur events
  function issuerValidation(){
    var err = true;
    if(!validateEmpId()){
      err=false;
    }
    if(!validateEmpEmail()){
      err=false;
    }
    if(!validateDesignation()){
      err=false;
    }
    if(!validateName()){
      err=false;
    }
    if(!validatePassword()){
      err=false;
    }
    return err;
  }
  
  // Authorizers registration :
  
  // EmpId validation :
  function validateEmpId1(){
    var empId = document.getElementById('id1');
                if(empId.value!=""){
                var res=valEmp(empId, "EmpId format incorrect","p6");
                if(res)
                return true;                
                }
                else{
                  document.getElementById('p6').innerText = "field mandatory";
                }
  }
  
  // Email validation :
  function validateEmpEmail1(){
    var email =  document.getElementById('email1');
    if(email.value!=""){
    var res=emailValidation(email, "Please enter a valid email address","p7");
    if(res)
      return true;
    }
    else{
      document.getElementById('p7').innerText = "field mandatory";
      return false;
    }
  }
  
  // Designation validation :
  function validateDesignation1(){
    var designation = document.getElementById('designation1');
      if(designation.value!=""){
        var res=valDesignation(designation, "For your designation please use alphabets only","p8");
      if(res)
       return true;
      }
      else{
        document.getElementById('p8').innerText = "field mandatory";
      }
  }
  
  // Name validation :
  function validateName1(){
    var name = document.getElementById('name1');
      if(name.value!=""){
        var res=valstring(name, "For your name please use alphabets only", "p9");
      if(res)
      return true;
      }
      else{
        document.getElementById('p9').innerText = "field mandatory";
      }
  }
  
  // Password validation :
  function validatePassword1(){
    var password = document.getElementById('password');
      if(password.value!=""){
        var res=passwordValidate(password, "Enter a password with atleast one symbol,digit,uppercase and of range 7 to 14", "p10");
      if(res)
      return true;
      }
      else{
        document.getElementById('p10').innerText = "field mandatory";
      }
  }
  
  // Authorizer registration form validation for Non-blur events
  function authorizerValidation(){
    var err = true;
    if(!validateEmpId1()){
      err=false;
    }
    if(!validateEmpEmail1()){
    err=false;
    }
    if(!validateDesignation1()){
    err=false;
    }
    if(!validateName1()){
    err=false;
    }
    if(!validatePassword1()){
      err=false;
    }
    return err;
  }
  
  // login validations
  async function loginValidation()
  {
    if(validateLoginEmail())
    {
      document.getElementById('Password').disabled=false;
      if(validatePassword())
      {
        document.getElementById('login').disabled=false;
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  async function validateLoginEmail(){
    var email =  document.getElementById('emailid');
    if(email.value!=""){
      var res=emailValidation(email, "Please enter a valid email address","p1")
      if(res)
      {
        return true;
      }
    }
    else{
        document.getElementById('p1').innerText ="Email cannot be empty";
        return false;
    }
}
  async function validatePassword(){
    var password = document.getElementById('Password');
    if(password.value.length>0){
      var res=passwordValidate(password, "Please provide valid password","p2");
        if(res)
        {
          return true;
        }
    }
  }

   //signup
   function signupValidation(){
    var password = document.getElementById('password');
    if(password.value.length == 0){
    document.getElementById('p3').innerText = "Password cannot be empty"; 
    password.focus();
    return false;
  } 
       if(validateCompany()){
              if(signupvalidateEmail()){
                  if(signupvalidatePassword()){
                      if(validateCpassword()){  
                          return true;
                          }
                      }
                  }
              }
      return false;	
  }
  function validateCompany(){
      var CompanyName =  document.getElementById('name');
      if(CompanyName.value!="")
      var res=inputAlphabet(CompanyName, "For your name please use alphabets only","p1");
      if(res){
          return true;
      }
      else{
      document.getElementById('p1').innerText = "field mandatory";
  }
  }
  function signupvalidateEmail(){
  var email =  document.getElementById('email');
  if(email.value!="")
  var res=emailValidation(email, "Please enter a valid email address","p2");
  if(res){
      return true;
  }
  else{
      document.getElementById('p2').innerText = "field mandatory";
      return false;
  }
  }
  function signupvalidatePassword(){
      var password = document.getElementById('password');
      if(password.value!="")
      var res=passwordValidate(password, "Please provide valid password","p3");
      if(res){
          return true;
      }
      else{
          return false;
      }
      }
  function validateCpassword(){
      var confirmpassword = document.getElementById('confirmpassword');
      if(confirmpassword.value!="")
      var res=passwordMatch(password,confirmpassword, "password and cpassword donot match","p4");
      if(res){
          return true;
      }
      else{
          return false;
      }
  }

  //dapp registration
  function dappvalidation(){
    var passphrase = document.getElementById('passphrase');
    var dappName = document.getElementById('dappName');
    var description = document.getElementById('description');
    var flag ="true";
    if(passphrase.value.length == 0){
    passphrase.focus();
    flag=false;
    }
    if(dappName.value.length == 0){ 
      passphrase.focus();
      flag=false;
    }
    if(description.value.length == 0){ 
      description.focus();
      flag=false;
    }
    if(flag=="true"){
      return true;
    }
    else{
      document.getElementById("head").innerText = "All Fields Mandatory";
      return false;
    }
    }
  
  //string validation
  function inputAlphabet(inputtext, alertMsg,id){
      var alphaExp = /^[a-zA-Z]+$/;
      if(inputtext.value.match(alphaExp)){
          document.getElementById(id).innerText = " ";
          return true;
      }
      else{
          document.getElementById(id).innerText = alertMsg;  
          inputtext.focus();
          return false;
      }
  }
  
  //Salary :
  function valSal(sal, alertMsg,id)
  {
    var salary = /^[0-9]+$/;
    if(sal.value.match(salary))
          {   
            document.getElementById(id).innerText = "";
            return true;
          }
        else
          {
            document.getElementById(id).innerText = alertMsg;
            sal.focus();
            return false;
          }
  }

  //Account number :
  function valAccNo(num, alertMsg,id)
  {
    var accno = /^[0-9]{15,18}$/;
    if(num.value.match(accno))
          {
            document.getElementById(id).innerText = "";
            return true;
          }
        else
          {
            document.getElementById(id).innerText = alertMsg;   
            num.focus();
            return false;
          }
  }

  //pan card:  
  function valPan(pan, alertMsg,id)
  {
    var pattern = /^([a-zA-Z]{5})([0-9]{4})([a-zA-Z]{1})$/;
    if(pan.value.match(pattern))
    { 
      document.getElementById(id).innerText = "";  
      return true;
    }
  else
    { 
      document.getElementById(id).innerText = alertMsg;
      pan.focus();
      return false; 
    }
    }

  //parword match
  function passwordMatch(inputtext1,inputtext2,alertMsg,id)
  {
    if(inputtext1.value===inputtext2.value)
    {
      document.getElementById(id).innerText = "";
      return true;
    }
    else{
      document.getElementById(id).innerText = alertMsg;
      inputtext2.focus();
      return false;
    }
  }
  
  //employee registration validation
  function empregformValidation(){
    if(empregvalidateEmail()){
           if(validateName2()){
         if(validateLname()){
               if(validateDes()){
                   if(validateBank()){
                   if(validateAcc()){
                     if(validatePan()){
                       if(validateSalary()){
                       return true;
                       }
                      }
                    }
                  }
                }
              }
            }
        }
      return false;	
  }

function empregvalidateEmail(){
  var email = document.getElementById('email');
  if(email.value!=""){
    var res=emailValidation(email, "Please enter a valid email address","p1")
    if(res)
    {
      return true;
    }
    }
    else{
      document.getElementById('p1').innerText = "cannot be empty"
    }
}

function validateLname(){
  var lastName =  document.getElementById('lastName');
    if(lastName.value!=""){
      var res=valstring(lastName, "For your name please use alphabets only","p2");
      if(res)
        return true;
      }
      else{
        document.getElementById('p2').innerText = "cannot be empty"
      }
    }
 
function validateName2(){
 var name = document.getElementById('name');
   if(name.value!=""){
     var res=valstring(name, "For your name please use alphabets only","p3");
   if(res)
   return true;
   }
   else{
     document.getElementById('p3').innerText = "cannot be empty"
   }
   }

function validateDes(){
 var designation = document.getElementById('designation');
       if(designation.value!=""){
         var res=valstring(designation, "only alphabets","p4");
       if(res)
       return true;
     }
     else{
       document.getElementById('p4').innerText = "cannot be empty"
     }
  }

function validateBank(){
 var bank = document.getElementById('bank');
         if(bank.value!=""){
           var res=valstring(bank, "only alphabets","p5")
         if(res)
         return true;
         }
         else{
           document.getElementById('p5').innerText = "cannot be empty"
         }
       }

function validateAcc(){
 var accountNumber = document.getElementById('accountNumber');
           if(accountNumber.value!=""){
             var res=valAccNo(accountNumber, "provide proper acc no","p6");
           if(res)
           return true;
             }
             else{
               document.getElementById('p6').innerText = "cannot be empty"
             }
           }

function validatePan(){
 var pan = document.getElementById('pan');
             if(pan.value!=""){
             var res=valPan(pan, "pan format not correct","p7");
             if(res)
               return true;
             }
             else{
               document.getElementById('p7').innerText = "cannot be empty"
             }
         }
        
function validateSalary(){
  var salary = document.getElementById('salary');
  if(salary.value!=""){
    var res=valSal(salary, "salary format incorrect","p8");
      if(res)
        return true;                
  }
  else{
    document.getElementById('p8').innerText = "cannot be empty"
      }
}




