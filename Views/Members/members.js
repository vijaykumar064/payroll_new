var dappid = "48a7d6bd571d8a636bfc7d64781e03e4dc80df75c99ca98788c63697f9a2d56a";
var prevTextBoxText = '';
var countryData, countryCode;
const count_countries = 253;
var registerData;
var role = localStorage.getItem("roleId");
var company = localStorage.getItem("companyname");
var email = localStorage.getItem("email");
var token = localStorage.getItem("belToken");
var dappid = localStorage.getItem("dappid");
var bel = localStorage.getItem("bel");
const iid = localStorage.getItem('issuerid');
var dep = localStorage.getItem("issuerdepartments");
console.log(iid);
setInterval(function () {
  searchBoxText = document.getElementById('search').value;
  if (searchBoxText != prevTextBoxText) {
    prevTextBoxText = searchBoxText;
    find(searchBoxText);
  }
}, 1000);
function model() {
  if ((role === "superuser") || (role === "new user")) {
    var list = document.getElementById('heading_list');
    //  list.childNodes[5].remove();
    //  list.childNodes[1].remove();
    list.childNodes[3].style.display = "block";
    list.childNodes[7].style.display = "block";
    // document.getElementById("reg").remove();
  }
  if(role==="superuser"){
    var list = document.getElementById('heading_list');
    list.childNodes[9].style.display = "block";
}
  if (role === "issuer") {
    var list = document.getElementById('heading_list');
    //  list.childNodes[5].remove();
    //  list.childNodes[3].remove();
    document.getElementById("issuerdepartments").innerText=dep;
    list.childNodes[1].style.display = "block";
    list.childNodes[7].style.display = "block";
    document.getElementById("reg").style.display = "block";
    var depdropdown = document.getElementById("department");

    var str=dep.split(',')
    for(i in str) {
        depdropdown.options[depdropdown.options.length] = new Option(str[i], i);
    }
  }
  if (role === "authorizer") {
    var list = document.getElementById('heading_list');
    //  list.childNodes[3].remove();
    //  list.childNodes[1].remove();
    list.childNodes[5].style.display = "block";
    list.childNodes[7].style.display = "block";
  }
  if((role==="issuer")||(role==="authorizer")){
    console.log("in issuer categor");
    // document.getElementById("category").innerText=category;
}



}
async function memberDashboard() {
  document.getElementById("name").innerText = email;
  document.getElementById("role").innerHTML = role.toUpperCase();
  document.getElementById("balance").innerText = (bel + " " + "BEL");
  if (role != "new user") {
    document.getElementById("company").innerText = company;
    const res1 = await getEmployee();
    console.log(res1);
    var n = res1.employees.length;
    if(n===0){
      document.getElementById('nomembers').style.display='block';
    }
    var i;
    for (i = 0; i < n; i++)
      add_employee(res1.employees[i].name, res1.employees[i].designation, res1.employees[i].empid);
  }
}

async function getEmployee() {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "employees",
      type: 'post',
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
    }
  }
}

function add_employee(emp_name, designation, emp_id) {
  add_div = '<div class="col-xl-4 col-lg-6 col-md-6 col-12 " id=' + emp_id + '><div class="col-xl-12 col-lg-12 col-md-12 col-12 member_views_details"><div class="col-xl-3 col-lg-3 col-md-3 col-12 nopadding"><a href=""><img class="propic" src="../../lib/img/profile.jpg" width="55"></a></div><div class="col-xl-6 col-lg-6 col-md-6 col-12 nopadding"><h6>' + emp_name + '</h6><p class="p1">' + designation + '</p><p class="p2">' + emp_id + '</p></div><div class="col-xl-3 col-lg-3 col-md-3 col-12 nopadding"></div></div></div>';
  document.getElementById('members_div').innerHTML += add_div;
}

async function find(text) {
  document.getElementById('members_div').innerHTML = "";
  const res2 = await findEmployee(text);
  //console.log(res2);
  var n = res2.result.length;
  console.log(n);
  var i;
  for (i = 0; i < n; i++) {
    add_employee(res2.result[i].name, res2.result[i].designation, res2.result[i].empid);
    console.log(res2.result[i].name + " " + res2.result[i].designation + " " + res2.result[i].empid);
  }
}

async function findEmployee(text) {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "searchEmployee",
      type: 'post',
      data: '{"text":"' + text + '","searchBy":"name","limit":10,"offset":0}',
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    setTimeout(function () { }, 500);
    console.log(result)
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
    }
  }
}

async function getBanksList()
{
    let banks;
    try {
        banks = await $.ajax({
            url: HOST_URL + "/" + dappid + "/" + "getBanks",
            type: 'post',
            data: JSON.stringify(registerData),
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        // banks={
        //     "banks": [
        //         "State Bank of India",
        //         "HDFC",
        //         "AXIS",
        //         "IDBI",
        //         "Other"
        //     ],
        //     "isSuccess": true,
        //     "success": true
        // };
        var banksDropdown = document.getElementById("rbank");
        if(banks.isSuccess===true)
        {
            for(i in banks.banks) {
                banksDropdown.options[banksDropdown.options.length] = new Option(banks.banks[i], i);
            }
            banksDropdown.options[banksDropdown.options.length] = new Option("Other", banksDropdown.options.length-2);

        }
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}


function getCountries() {
  $.get("http://54.254.174.74:8080/api/v1/countries", function (data) {
    // console.log(data);
    countryData = data;
    var x = document.getElementById("countrycode");
    for (var i = 0; i < count_countries; i++) {
      var option = document.createElement("option");
      option.text = countryData['data'][i]['countryName'];
      x.add(option);
    }
  });
}

function countryValidator() {
  var e = document.getElementById("countrycode");
  // console.log(e);
  var strUser = e.options[e.selectedIndex].text;
  // console.log(e.options[e.selectedIndex]);
  var i = e.selectedIndex - 1;
  //console.log(strUser);
  console.log(countryData['data'][i]['countryCode']);
  countryCode = countryData['data'][i]['countryCode'];
  console.log(countryData['data'][i]['countryID']);
}

async function register() {
  var bankName,designation;
  console.log(identityproof)
  var idvalue=document.getElementById("pannum").value
  var identity1=createidentityobject(identityproof,idvalue)
  var e = document.getElementById("department");
  var department=e.options[e.selectedIndex].text;
// alert(bankName);
console.log(id);
// alert(id);
if(id==="rbank"){
  e=document.getElementById("rbank")
  bankName=e.options[e.selectedIndex].text;
}
else if(id==="otherbank"){
  bankName=document.getElementById("otherbank").value;;
}
if(desid==="designation1"){
  var e = document.getElementById("designation1");
designation=e.options[e.selectedIndex].text;
}
else if(desid==="otherdes"){
  designation=document.getElementById("otherdes").value;
}
  registerData = {
    countryCode: countryCode,
    email: document.getElementById("email").value,
    lastName: document.getElementById("lname").value,
    name: document.getElementById("fname").value,
    empid: document.getElementById("empid1").value,
    designation:designation,
    bank: bankName,
    accountNumber: document.getElementById("accnum").value,
    identity:identity1,
    salary: document.getElementById("salary1").value,
    dappid: dappid,
    token: token,
    groupName: "Dapps",
    iid:iid,
    department:department
  };
  var flag=await empregformValidation();
  if(flag===true){
    $.blockUI({ message: '<i  class="fa fa-circle-o-notch fa-spin" style="z-index: 99999 !important; font-size:70px; color: #00bfff"></i>',css: {backgroundColor: 'transparent', border: '0'} });
  console.log(registerData);
  const res3 = await registerEmployee();
  console.log(res3);
  if (res3.isSuccess === true) {    
    // document.getElementById("close").click();
    document.getElementById("registered").click();
  }
  else{
    if(res3.message==="Employee with the same identity already exists")
    {
    document.getElementById("msg").innerText=res3.message;
    $("#reg-new-emp").modal('show');
    }
    else{
    // document.getElementById("close").click();
    $("#resmsg").text(JSON.parse(res3.message).message);
    $("#responsemsg").modal('show');
    }    
  }
  $.unblockUI();
}
}
function tagclose() {
  document.getElementById("issuer").click();
}
// function for registering new employee
async function registerEmployee() {
  let result;
  try {
    result = await $.ajax({
      url: HOST_URL + "/" + dappid + "/" + "registerEmployee",
      type: 'post',
      data: JSON.stringify(registerData),
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json'
    });
    return result;
  }
  catch (error) {
    if (error) {
      console.log(error);
    }
  }
}


function createidentityobject(identity,num){
  var obj={}

   key=identity;
   obj[key]=num;
return obj;
}
// async function getdepList(){
//   console.log("in dep list")
//   let result;
//   try {
//      result = await $.ajax({
//           url: HOST_URL + "/" + dappid + "/department/get",
//           type: 'post',
//           contentType: 'application/json;charset=UTF-8',
//           dataType: 'json'
//       });
//       console.log(JSON.stringify(result)+"sdrtfgyuhji");
      
//       var depdropdown = document.getElementById("department");
//       if(result.isSuccess===true)
//       {
//           for(i in result.departments) {
//               depdropdown.options[depdropdown.options.length] = new Option(result.departments[i].name, i);
//           }
//       }
//   }
//   catch (error) {
//       if (error) {
//           console.log(error);
//       }
//   }
// }


async function getdesList()
{    
    let result;
    try {
       result = await $.ajax({
            url: HOST_URL + "/" + dappid + "/employees/getDesignations",
            type: 'post',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json'
        });
        console.log(JSON.stringify(result)+"sdrtfgyuhji");
        var desdropdown = document.getElementById("designation1");
        if(result.isSuccess===true)
        {
            for(i in result.designation) {
                desdropdown.options[desdropdown.options.length] = new Option(result.designation[i], i);
            }
            desdropdown.options[desdropdown.options.length] = new Option("Other", desdropdown.options.length-2);
        }
    }
    catch (error) {
        if (error) {
            console.log(error);
        }
    }
}


var id;
function bankValidator()
{
    console.log("in bank validator");
    var banksDropdown = document.getElementById("rbank");
    var selectedBank=banksDropdown.options[banksDropdown.selectedIndex].text;
    if(selectedBank==="Other")
    {
        console.log("edftghujk")
        // document.getElementById("rbank").style.display = "none";
        document.getElementById("otherbank").style.display = "block";
        id="otherbank";
        // bankName=document.getElementById("otherbank").value;
    }
    else
    {
        document.getElementById("otherbank").style.display = "none";
        // bankName=banksDropdown.options[banksDropdown.selectedIndex].text;
        id="rbank"
    }
}
var desid;
function desvalidator(){
    var e = document.getElementById("designation1");
    var des=e.options[e.selectedIndex].text;
    if(des==="Other")
    {
        desid="otherdes"
        // document.getElementById("rbank").style.display = "none";
        document.getElementById("otherdes").style.display = "block";
        // designation=document.getElementById("otherdes").value;
    }
    else
    {
        desid="designation1"
        document.getElementById("otherdes").style.display = "none";
        // designation=des;
    }
}


async function getcustomfields(){
  //http://localhost:9305/api/dapps/6f5197bb89daae4b28014eb3236492954472cfe5403b9f20dc7dce96d014a0a9/customFields/get
  let result;
  try {
      result = await $.ajax({
          url: HOST_URL + "/" + dappid + "/customFields/get",
          type: 'post',
          contentType: 'application/json;charset=UTF-8',
          dataType: 'json'
      });
      // console.log(result);
      if(result.isSuccess===true){
          key3=result.identity
          for(p in key3) {
              // console.log (p);
              // $("#pan").attr("placeholder",p);
               identityproof=p;
              $("#pannum").attr("placeholder",p);     
          }

      }
      // return result;
  }
  catch (error) {
      if (error) {
          console.log(error);
      }
  }  

}